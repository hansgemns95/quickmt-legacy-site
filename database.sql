-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Gegenereerd op: 20 nov 2020 om 21:37
-- Serverversie: 5.6.49
-- PHP-versie: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quickmtf_qck`
--
CREATE DATABASE IF NOT EXISTS `quickmtf_qck` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `quickmtf_qck`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `active`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `active`;
CREATE TABLE `active` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `afwezigheid`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `afwezigheid`;
CREATE TABLE `afwezigheid` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `start_date` int(11) NOT NULL,
  `end_date` int(11) NOT NULL,
  `status` enum('in behandeling','geaccepteerd','geweigerd') NOT NULL DEFAULT 'in behandeling',
  `handled_by` int(11) NOT NULL DEFAULT '0',
  `reason` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Tabelstructuur voor tabel `belasting`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `belasting`;
CREATE TABLE `belasting` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `created_on` int(11) NOT NULL,
  `accepted_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `bonus`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `bonus`;
CREATE TABLE `bonus` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` varchar(50) NOT NULL,
  `total` double NOT NULL,
  `omschrijving` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `language`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `section` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `language`
--

INSERT INTO `language` (`id`, `key`, `section`, `value`) VALUES
(1, 'admin_home_blocked_login', 'failed_logins', 'Mislukte Inlogpogingen'),
(3, 'admin_home_blocked_login', 'last_15_min', 'Afgelopen Kwartier'),
(4, 'admin_home_blocked_login', 'last_hour', 'Afgelopen Uur'),
(5, 'admin_home_blocked_login', 'last_day', 'Afgelopen Dag'),
(6, 'admin_home_blocked_login', 'last_week', 'Afgelopen Week'),
(7, 'admin_home_global_stats', 'global_stats', 'Globale Statistieken'),
(8, 'admin_home_global_stats', 'total_org', 'Totaal Aantal Organisaties'),
(9, 'admin_home_global_stats', 'total_users', 'Totaal Aantal Gebruikers'),
(10, 'admin_home_global_stats', 'total_products', 'Totaal Aantal Producten'),
(11, 'admin_home_global_stats', 'total_perms', 'Totaal Aantal Permissies'),
(12, 'admin_home_header', 'header', 'Homepage'),
(13, 'admin_home_header', 'sub_header', 'Mochten er velden {{red}} rood {{/red}} zijn neem dan z.s.m. contact op met Julian Meurer'),
(14, 'admin_home_logs_stats', 'logs_ammount', 'Aantal Logboek Toevoegingen'),
(15, 'admin_home_logs_stats', 'last_15_min', 'Afgelopen Kwartier'),
(16, 'admin_home_logs_stats', 'last_hour', 'Afgelopen Uur'),
(17, 'admin_home_logs_stats', 'last_day', 'Afgelopen Dag'),
(18, 'admin_home_logs_stats', 'last_week', 'Afgelopen Week'),
(19, 'admin_organisations_header', 'org_add', 'Organisatie Toevoegen'),
(20, 'admin_organisations_header', 'sub_header', 'Hieronder worden alle organisaties weergeven en kunt u nieuwe organisaties aanmaken'),
(21, 'admin_organisations_information', 'org_info', 'Organisatie Informatie'),
(22, 'admin_organisations_information', 'org_info_id', 'ID:'),
(23, 'admin_organisations_information', 'org_info_name', 'Naam:'),
(24, 'admin_organisations_information', 'org_info_location', 'Locatie:'),
(25, 'admin_organisations_information', 'org_info_deleted_on', 'Verwijderd Op:'),
(26, 'admin_organisations_information', 'org_info_deleted_by', 'Verwijderd Door:'),
(27, 'admin_organisations_information', 'org_info_created_on', 'Aangemaakt Op:'),
(28, 'admin_organisations_information', 'org_info_created_by', 'Aangemaakt Door:'),
(29, 'admin_organisations_information', 'org_info_last_change_name', 'Laatste Naam Wijziging:'),
(30, 'admin_organisations_information', 'org_info_last_change_location', 'Laatste Locatie Wijziging:'),
(31, 'admin_organisations_information', 'org_info_cannot_load', 'De organisatie kan niet worden geladen'),
(32, 'admin_organisations_information', 'org_delete', 'Organisatie Verwijderen'),
(33, 'admin_organisations_location', 'org_location_change', 'Organisatie Locatie Aanpassen'),
(34, 'admin_organisations_location', 'org_location_cannot_load', 'De organisatie kan momenteel niet worden geladen'),
(35, 'admin_organisations_location', 'org_location_cannot_edit_admin', 'De admin organisatie kan niet worden aangepast'),
(36, 'admin_organisations_location', 'org_location_cannot_edit_deleted', 'De organisatie kan niet worden aangepast aangezien deze is verwijderd'),
(37, 'admin_organisations_location', 'org_location_changed', 'De organisatie locatie is aangepast naar: '),
(38, 'admin_organisations_location', 'org_location_error', 'De organisatie kan niet worden gewijzigd'),
(39, 'admin_organisations_location', 'org_location_empty', 'De ingevoerde locatie is leeg, probeer het later nog eens'),
(40, 'admin_organisations_location', 'org_location_current', 'Huidige Locatie:'),
(41, 'admin_organisations_location', 'org_location_input_placeholder', 'Nieuwe Locatie'),
(42, 'admin_organisations_location', 'org_location_submit', 'Aanpassen'),
(43, 'admin_organisations_name', 'org_name_change', 'Organisatie Naam Aanpassen'),
(44, 'admin_organisations_name', 'org_name_empty', 'De organisatie kan momenteel niet worden geladen'),
(45, 'admin_organisations_name', 'org_name_admin', 'De admin organisatie kan niet worden aangepast'),
(46, 'admin_organisations_name', 'org_name_deleted', 'De organisatie kan niet worden aangepast aangezien deze is verwijderd'),
(47, 'admin_organisations_name', 'org_name_changed', 'De organisatie naam is aangepast naar:'),
(48, 'admin_organisations_name', 'org_name_error', 'De organisatie kan niet worden gewijzigd'),
(49, 'admin_organisations_name', 'org_name_empty_input', 'De ingevoerde naam is leeg, probeer het later nog eens'),
(50, 'admin_organisations_name', 'org_name_taken', 'Er bestaat al een organisatie met deze naam'),
(51, 'admin_organisations_name', 'org_name_submit', 'Aanpassen'),
(52, 'admin_organisations_name', 'org_name_current', 'Huidige Naam:'),
(53, 'admin_organisations_name', 'org_name_input_placeholder', 'Nieuwe Naam'),
(54, 'admin_organisations_tabel', 'org_create_error', 'De nieuwe organisatie kan momenteel niet worden gemaakt'),
(55, 'admin_organisations_tabel', 'org_delete_error', 'De organisatie kan momenteel niet worden verwijderd'),
(56, 'admin_organisations_tabel', 'org_deleted', 'De geselecteerde organisatie is succesvol verwijderd'),
(57, 'admin_organisations_tabel', 'org_created', 'De nieuwe organisatie is succesvol aangemaakt'),
(58, 'admin_organisations_tabel', 'org_duplicate', 'Er bestaat al een organisatie met deze naam'),
(59, 'admin_organisations_tabel', 'org_info', 'Organisaties'),
(60, 'admin_organisations_tabel', 'org_info_id', 'ID'),
(61, 'admin_organisations_tabel', 'org_info_name', 'Naam'),
(62, 'admin_organisations_tabel', 'org_info_location', 'Locatie'),
(63, 'admin_organisations_tabel', 'org_info_active', 'Actief'),
(64, 'admin_organisations_tabel', 'org_info_cannot_load', 'De organisaties kunnen momenteel niet worden geladen'),
(65, 'admin_user_code', 'user_personal_code', 'Persoonlijke Code Resetten'),
(66, 'admin_user_code', 'user_personal_code_cannot_load', 'De gebruiker kan niet worden geladen'),
(67, 'admin_user_code', 'user_personal_code_deleted', 'Deze functie is uitgeschakeld bij verwijderde gebruikers'),
(68, 'admin_user_code', 'user_personal_code_own', 'Het is niet mogelijk via dit menu je eigen code te resetten'),
(69, 'admin_user_code', 'user_personal_code_admin', 'Het is niet mogelijk via dit menu een bewerking uit te voeren bij een admin'),
(70, 'admin_user_code', 'user_personal_code_error', 'De code kon niet worden gereset'),
(71, 'admin_user_code', 'user_personal_code_reset', 'De code is gereset. De nieuwe code is '),
(72, 'admin_user_code', 'user_personal_code_submit', 'Persoonlijke Code Resetten'),
(73, 'admin_user_header', 'user_add', 'Gebruiker Toevoegen'),
(74, 'admin_user_header', 'sub_header', 'Hieronder worden alle gebruikers weergeven en kunt u nieuwe gebruikers toevoegen'),
(75, 'admin_user_header', 'header', 'Gebruikers'),
(76, 'admin_organisations_tabel', 'org_info_fantasy', 'Fantasy'),
(77, 'admin_organisations_information', 'org_fantasy_enable', 'Fantasy Modus Inschakelen'),
(78, 'admin_organisations_information', 'org_fantasy_disable', 'Fantasy Modus Uitschakelen'),
(79, 'admin_organisations_information', 'org_fantasy', 'Fantasy Modus'),
(80, 'admin_organisations_information', 'org_fantasy_info', 'Wanneer fantasy modus staat ingeschakeld wordt de organisatie gezien als een niet fysiek bestaand bedrijf. Dit betekend onder andere dat het bedrijf niet op te vragen is bij via de API'),
(81, 'admin_organisations_icon', 'org_icon_change', 'Organisatie Icon Aanpassen'),
(82, 'admin_organisations_icon', 'org_icon_cannot_load', 'De organisatie kan momenteel niet worden geladen'),
(83, 'admin_organisations_icon', 'org_icon_cannot_edit_deleted', 'De admin organisatie kan niet worden aangepast'),
(84, 'admin_organisations_icon', 'org_icon_changed', 'De organisatie icon is aangepast naar: '),
(85, 'admin_organisations_icon', 'org_icon_error', 'De organisatie kan niet worden gewijzigd'),
(86, 'admin_organisations_icon', 'org_icon_empty', 'De ingevoerde icon is leeg, probeer het later nog eens'),
(87, 'admin_organisations_icon', 'org_icon_current', 'Huidige Icon:'),
(88, 'admin_organisations_icon', 'org_icon_input_placeholder', 'Nieuwe Icon'),
(89, 'admin_organisations_icon', 'org_icon_submit', 'Aanpassen'),
(90, 'admin_organisations_icon', 'org_icon_cannot_edit_admin', 'De admin organisatie kan niet worden aangepast'),
(91, 'admin_organisations_icon', 'org_icon_hint', '{{italic}}Het is belangrijk dat de icon begint en eindigt met een {{bold}}:{{/bold}}{{/italic}}'),
(92, 'admin_organisations_information', 'org_info_last_change_icon', 'Laatste Icon Wijziging:'),
(93, 'admin_user_admin', 'user_admin_enable', 'Admin Modus Inschakelen'),
(94, 'admin_user_admin', 'user_admin_disable', 'Admin Modus Uitschakelen'),
(95, 'admin_user_admin', 'user_admin', 'Admin Modus'),
(96, 'admin_user_admin', 'user_admin_info', 'Admin modus zorgt ervoor dat een gebruiker organisaties en gebruikers aan kan maken. {{br}}{{bold}}{{red}}Let op: Een gebruiker kan hierdoor geen verkoop of administratie taken meer uitvoeren bij een winkel, alleen nog globaal{{/red}}{{/bold}}'),
(97, 'admin_user_admin', 'user_admin_cannot_load', 'De gebruiker kan niet worden geladen'),
(98, 'admin_user_admin', 'user_admin_own', 'Het is niet mogelijk via dit menu je eigen status aan te resetten'),
(99, 'admin_organisations_showOpen', 'org_showOpen', 'Open Modus'),
(100, 'admin_organisations_showOpen', 'org_showOpen_info', 'Wanneer open modus aan staat is de organisatie bereikbaar via de getStatus api.'),
(101, 'admin_organisations_showOpen', 'org_showOpen_enable', 'open Modus Inschakelen'),
(102, 'admin_organisations_showOpen', 'org_showOpen_disable', 'Open Modus Uitschakelen'),
(103, 'admin_organisations_tabel', 'org_info_showOpen', 'Open');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `login_attempts_ip`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `login_attempts_ip`;
CREATE TABLE `login_attempts_ip` (
  `ip` varchar(100) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Tabelstructuur voor tabel `logs`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `time` varchar(50) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `lonen`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `lonen`;
CREATE TABLE `lonen` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total` double NOT NULL,
  `time` varchar(50) NOT NULL,
  `calculatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `meldingen`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `meldingen`;
CREATE TABLE `meldingen` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  `prioriteit` enum('low','medium','high','urgent') NOT NULL,
  `icon` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `gelezen` tinyint(1) NOT NULL DEFAULT '0',
  `melding_group_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `mutatieLogs`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `mutatieLogs`;
CREATE TABLE `mutatieLogs` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `time_id` int(11) NOT NULL,
  `time` varchar(50) NOT NULL,
  `omzet` double NOT NULL,
  `kas` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `mutaties`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `mutaties`;
CREATE TABLE `mutaties` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `type` enum('in','out') NOT NULL,
  `time` varchar(50) NOT NULL,
  `total` double NOT NULL,
  `omschrijving` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `notities`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `notities`;
CREATE TABLE `notities` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `titel` varchar(100) NOT NULL,
  `text` text NOT NULL,
  `createdOn` varchar(50) NOT NULL,
  `deletedOn` varchar(50) NOT NULL DEFAULT '0',
  `deletedBy` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Tabelstructuur voor tabel `organisation`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `organisation`;
CREATE TABLE `organisation` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `showOpen` tinyint(1) NOT NULL DEFAULT '0',
  `fantasy` tinyint(1) NOT NULL DEFAULT '0',
  `partner` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `organisation`
--

INSERT INTO `organisation` (`id`, `name`, `location`, `icon`, `active`, `showOpen`, `fantasy`, `partner`) VALUES
(1, 'Admin', 'Cloud', ':cloud:', 1, 0, 1, 0);


--
-- Tabelstructuur voor tabel `organisationStats`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `organisationStats`;
CREATE TABLE `organisationStats` (
  `org_id` int(10) NOT NULL,
  `createdOn` varchar(50) NOT NULL,
  `createdBy` int(10) NOT NULL,
  `deletedOn` varchar(50) NOT NULL DEFAULT '0',
  `deletedBy` int(10) NOT NULL DEFAULT '0',
  `belastingPercentage` int(11) DEFAULT NULL,
  `lastNameChange` varchar(50) NOT NULL,
  `lastLocationChange` varchar(50) NOT NULL,
  `lastLoonReset` varchar(50) NOT NULL DEFAULT '0',
  `lastActiveReport` varchar(50) NOT NULL DEFAULT '0',
  `lastActiveReportBy` int(11) NOT NULL DEFAULT '0',
  `lastIconChange` int(50) NOT NULL DEFAULT '0',
  `MutatieID` int(11) NOT NULL DEFAULT '1',
  `lastVoorraadCheckedOn` int(11) NOT NULL,
  `lastVoorraadCheckedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `organisationStats`
--

INSERT INTO `organisationStats` (`org_id`, `createdOn`, `createdBy`, `deletedOn`, `deletedBy`, `belastingPercentage`, `lastNameChange`, `lastLocationChange`, `lastLoonReset`, `lastActiveReport`, `lastActiveReportBy`, `lastIconChange`, `MutatieID`, `lastVoorraadCheckedOn`, `lastVoorraadCheckedBy`) VALUES
(1, '1582828128', 1, '0', 0, NULL, '1582933325', '1582933312', '0', '0', 0, 0, 1, 0, 0);

--
-- Tabelstructuur voor tabel `orgPages`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `orgPages`;
CREATE TABLE `orgPages` (
  `org_id` int(10) NOT NULL,
  `page_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Tabelstructuur voor tabel `pages`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `intern` tinyint(1) NOT NULL DEFAULT '1',
  `partner` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `pages`
--

INSERT INTO `pages` (`id`, `name`, `description`, `active`, `intern`, `partner`) VALUES
(1, 'Verkoop', 'Deze pagina geeft de mogelijkheid tot invoeren van verkopen en het inzien van het loon & resterende doel van een gebruiker (Gebruiker gebonden).', 1, 1, 1),
(2, 'Voorraad', '', 0, 0, 0),
(3, 'Overzicht - Voorraad', 'Deze pagina geeft de mogelijkheid tot inzien, bijhouden en bewerken van de voorraad.', 1, 1, 1),
(4, 'Overzicht - Omzet', 'Deze pagina geeft de mogelijkheid tot inzien van de gehele omzet en van de omzet per gebruiker (Gepind & Cash geld gescheiden) per huidige loonperiode, per week, per maand & in zijn geheel. ', 1, 1, 1),
(5, 'Overzicht - Verkopen', 'Deze pagina geeft de mogelijkheid tot inzien en verwijderen van alle verkopen ooit geregistreerd.', 1, 1, 1),
(6, 'Overzicht - Lonen', 'Deze pagina geeft de mogelijkheid tot inzien van alle geregistreerde lonen per gebruiker &  een actueel overzicht en het bereken van de definitieve lonen en hierbij het beïndigen van een loonperiode.', 1, 1, 1),
(7, 'Notities', 'Deze pagina geeft de mogelijkheid tot inzien, aanmaken en verwijderen van notities.', 1, 1, 1),
(8, 'Overzicht - Inkomsten/Uitgaven', 'Deze pagina geeft de mogelijkheid tot het inzien, aanmaken en verwijderen van inkomsten & uitgaven. Aan de hand van deze gegevens wordt de staatskas en het overzicht op het dasboard berekend.', 1, 1, 1),
(9, 'Overzicht - Persoonlijk Doel', 'Deze pagina geeft de mogelijkheid tot inzien en aanpassen van doelen per gebruiker.', 1, 1, 1),
(10, 'Overzicht - Bonus', 'Deze pagina geeft de mogelijkheid tot inzien, toevoegen en verwijderen van bonussen per gebruiker.', 1, 1, 1),
(11, 'Overzicht - Activiteiten', 'Deze pagina geeft de mogelijkheid tot inzien van de geregistreerde activiteit van gebruikers.', 1, 1, 1),
(12, 'Overzicht - Mededelingen', '', 0, 0, 0),
(13, 'Planning', 'Deze pagina geeft de mogelijkheid tot het invullen en aanpassen van de planning. ', 1, 1, 1),
(14, 'Overzicht - Sollicitatie', 'Deze pagina geeft de mogelijkheid tot inzien en archiveren van ingediende sollicitaties.', 1, 1, 0),
(15, 'Afwezigheid', 'Deze pagina geeft de mogelijkheid tot het afwezig melden van gebruikers.', 1, 1, 1),
(16, 'Overzicht - Afwezigheid', 'Deze pagina geeft de mogelijkheid tot goed- en afkeuren van afwezigheidsmeldingen van gebruikers.', 1, 1, 1);

-- --------------------------------------------------------


--
-- Tabelstructuur voor tabel `pendingUserDelete`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `pendingUserDelete`;
CREATE TABLE `pendingUserDelete` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `permissions`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(250) NOT NULL,
  `intern` tinyint(1) NOT NULL DEFAULT '1',
  `partner` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `description`, `intern`, `partner`) VALUES
(1, '*', 'Deze permissie geeft een persoon toegang tot alle permissies met hierbij ook alle toekomstige permissies.', 1, 1),
(2, 'page.admin.users', 'Deze permissie geeft het recht tot inzien van de pagina met alle gebruikers en gebonden permissies.', 1, 1),
(3, 'page.admin.users.manage', 'Deze permissie geeft het recht tot het aanpassen van permissies van gebruikers.', 1, 1),
(6, 'page.admin.pages', 'Deze permissie geeft het recht tot inzien van een overzicht van pagina\'s.', 1, 1),
(7, 'page.admin.pages.manage', 'Deze permissie geeft het recht tot het activeren en uitschakelen van pagina\'s. Bij het uitschakelen van een pagina kan er data verloren gaan.', 1, 1),
(8, 'page.admin.products', 'Deze permissie geeft het recht tot het inzien van een overzicht van alle producten.', 1, 1),
(9, 'page.admin.products.manage', 'Deze permissie geeft het recht tot het verwijderen & toevoegen van (bestaande) producten maar ook het aanpassen van producten (Prijs & Naam)', 1, 1),
(10, 'page.admin.users.add', 'Deze permissie geeft het recht tot  het toevoegen van (bestaande) gebruikers.', 1, 1),
(11, 'page.admin.users.remove', 'Deze permissie geeft het recht tot  het verwijderen van (bestaande) gebruikers. Het account blijft bestaan totdat er een aanvraag wordt gedaan tot deactivatie van het account.', 1, 1),
(16, 'page.verkoop', 'Deze permissie geeft het recht tot inzien van de verkooppagina en hierbij het invullen van verkopen. Hieronder valt ook het inzien van het resterende doel & gekregen loon. (Dit is persoonsgebonden.)', 1, 1),
(17, 'page.voorraad', '', 0, 0),
(18, 'page.overzicht.voorraad', 'Deze permissie geeft het recht tot inzien van de voorraad pagina en hierbij de huidige voorraad.', 1, 1),
(19, 'page.overzicht.omzet', 'Deze permissie geeft het recht tot inzien van de omzet pagina en hierbij de omzet van (alle) gebruikers.', 1, 1),
(20, 'page.overzicht.verkopen', 'Deze permissie geeft het recht tot inzien van de verkopen pagina en hierbij het bekijken alle geregistreerd verkopen per gebruiker.', 1, 1),
(21, 'page.overzicht.voorraad.manage', 'Deze permissie geeft het recht tot aanpassen van de voorraad.', 1, 1),
(22, 'page.overzicht.lonen', 'Deze permissie geeft het recht tot inzien van de lonen pagina en hierbij het bekijken van alle geregistreerde lonen per gebruiker.', 1, 1),
(23, 'page.overzicht.lonen.berekenen', 'Deze permissie geeft het recht tot het berekenen van de definitieve lonen.', 1, 1),
(24, 'loon.percentage.125', 'Deze permissie geeft het recht tot een loonpercentage van 12,5%.', 1, 0),
(25, 'loon.percentage.11', 'Deze permissie geeft het recht tot een loonpercentage van 11%.', 1, 0),
(26, 'page.overzicht.verkopen.manage', 'Deze permissie geeft het recht tot verwijderen van verkopen binnen de huidige loonperiode.', 1, 1),
(27, 'page.notities', 'Deze permissie geeft het recht tot inzien van de notitie pagina en hierbij alle notities.', 1, 1),
(28, 'page.notities.add', 'Deze permissie geeft het recht tot aanmaken van notities.', 1, 1),
(29, 'page.notities.remove', 'Deze permissie geeft het recht tot verwijderen van notities.', 1, 1),
(30, 'page.overzicht.in_out', 'Deze permissie geeft het recht tot bekijken van de inkomsten & uitgaven pagina en hierbij het bekijken van alle geregistreerde inkomsten & uitgaven.', 1, 1),
(31, 'page.overzicht.in_out.add', 'Deze permissie geeft het recht tot toevoegen van inkomsten & uitgaven.', 1, 1),
(32, 'page.overzicht.in_out.remove', 'Deze permissie geeft het recht tot verwijderen van inkomsten & uitgaven binnen de huidige loonperiode.', 1, 1),
(33, 'page.home.stats', 'Deze permissie geeft het recht tot inzien van de statistieke & grafieken op de home pagina.', 1, 1),
(34, 'loon.percentage.0', 'Deze permissie geeft het recht tot een loonpercentage van 0%.', 1, 0),
(35, 'page.overzicht.doel', 'Deze permissie geeft het recht tot inzien van de doelen pagina en hierbij het inzien van alle huidige opgestelde doelen per gebruiker.', 1, 1),
(36, 'page.overzicht.doel.manage', 'Deze permissie geeft het recht tot aanpassen van alle doelen per gebruiker.', 1, 1),
(37, 'page.overzicht.bonus', 'Deze permissie geeft het recht tot inzien van de bonussen pagina en hierbij alle geregistreerde bonussen per gebruiker.', 1, 1),
(38, 'page.overzicht.bonus.add', 'Deze permissie geeft het recht tot toevoegen van bonussen  per gebruiker.', 1, 1),
(39, 'page.overzicht.bonus.remove', 'Deze permissie geeft het recht tot verwijderen van bonussen per gebruiker binnen  de huidige loonperiode.', 1, 1),
(40, 'page.overzicht.activity', 'Deze permissie geeft het recht tot inzien van de activiteiten pagina en hierbij alle geregistreerde activiteit per gebruiker.', 1, 1),
(41, 'page.overzicht.mededelingen', '', 0, 0),
(42, 'page.planning', 'Deze permissie geeft het recht tot inzien van de planning pagina en hierbij het opgeven op de planning.', 1, 1),
(43, 'page.planning.manage', 'Deze permissie geeft het recht tot inzien van de sollicitatie paginas en hierbij alle ingeleverde sollicitatie. Ook is het mogelijk om met deze permissies sollicitaties te archiveren.', 1, 1),
(48, 'page.overzicht.sollicitatie', 'Deze permissie geeft het recht tot inzien van de sollicitatie paginas en hierbij alle ingeleverde sollicitatie. Ook is het mogelijk om met deze permissies sollicitaties te archiveren.', 1, 0),
(49, 'page.overzicht.afwezigheid', 'Deze permissie geeft het recht tot inzien van de afwezigheid pagina, die valt onder het overzicht tabje.', 1, 1),
(50, 'page.afwezigheid', 'Deze permissie geeft het recht tot het melden van een afwezigheid. Bij een goedgekeurde afwezigheid kan een persoon zich niet meer inplannen. ', 1, 1),
(51, 'page.overzicht.afwezigheid.manage', 'Deze permissie geeft het recht tot het goed- en afkeuren van afwezigheden.', 1, 1),
(53, 'page.leaderboard.medwVanDeMaand', 'Deze permissie geeft het recht tot inzien van het Medw. van de Maand levelboard op de scorelijst pagina.', 1, 0);

-- --------------------------------------------------------
--
-- Tabelstructuur voor tabel `personalChallenge`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `personalChallenge`;
CREATE TABLE `personalChallenge` (
  `org_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `challenge` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `planning`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `planning`;
CREATE TABLE `planning` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` varchar(255) NOT NULL,
  `slot_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `products`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `isLimited` tinyint(1) NOT NULL DEFAULT '0',
  `limit` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `sollicitatie`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `sollicitatie`;
CREATE TABLE `sollicitatie` (
  `id` int(11) NOT NULL,
  `status` enum('pending','archive') NOT NULL DEFAULT 'pending',
  `handledBy` int(11) NOT NULL DEFAULT '0',
  `handledOn` int(11) NOT NULL DEFAULT '0',
  `functie` enum('medw','dev') NOT NULL,
  `bedrijf` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `tutorial` tinyint(1) NOT NULL,
  `online_time` varchar(50) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `online_time_day` varchar(50) NOT NULL,
  `online_time_day_comp` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL,
  `city_registered` varchar(255) NOT NULL,
  `leeftijd` varchar(50) NOT NULL,
  `contact_social` enum('ja','nee','no_voice') NOT NULL,
  `job` tinyint(1) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `werkervaring` varchar(255) NOT NULL,
  `VOG` enum('ja','nee','idk') NOT NULL,
  `motivatie` varchar(255) NOT NULL,
  `contact` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `user`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `code` int(8) NOT NULL,
  `totp` varchar(255) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `code`, `totp`, `name`, `admin`, `active`) VALUES
(1, 'admin', '$2y$04$wYKvtHLYqrGcXHEnv9k6S.JV40Cr550kowe2GC7MRJMTI.ZxMVjne', 123456, NULL, 'Beheer', 1, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `userLoonPercentage`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `userLoonPercentage`;
CREATE TABLE `userLoonPercentage` (
  `org_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `loonpercentage` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `userOrganisation`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `userOrganisation`;
CREATE TABLE `userOrganisation` (
  `user_id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `userPerms`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `userPerms`;
CREATE TABLE `userPerms` (
  `user_id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `perm_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `userStats`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `userStats`;
CREATE TABLE `userStats` (
  `user_id` int(10) NOT NULL,
  `createdOn` varchar(50) NOT NULL,
  `createdBy` int(10) NOT NULL,
  `deletedOn` varchar(50) NOT NULL DEFAULT '0',
  `deletedBy` int(10) NOT NULL DEFAULT '0',
  `lastCodeReset` varchar(50) NOT NULL,
  `lastNameUpdate` varchar(50) NOT NULL,
  `lastPasswordUpdate` varchar(50) NOT NULL,
  `lastLevelCheck` varchar(50) NOT NULL DEFAULT '0',
  `lastMedwVanDeMaandPuntenCheck` varchar(50) DEFAULT '0',
  `level` int(11) NOT NULL DEFAULT '1',
  `medwVanDeMaandPunten` int(11) NOT NULL DEFAULT '1',
  `requiredToUseChrome` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `userStats`
--

INSERT INTO `userStats` (`user_id`, `createdOn`, `createdBy`, `deletedOn`, `deletedBy`, `lastCodeReset`, `lastNameUpdate`, `lastPasswordUpdate`, `lastLevelCheck`, `lastMedwVanDeMaandPuntenCheck`, `level`, `medwVanDeMaandPunten`, `requiredToUseChrome`) VALUES
(1, '1582931614', 1, '0', 0, '1582931614', '1582932553', '1582931614', '0', '0', 0, 1, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `verkopen`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `verkopen`;
CREATE TABLE `verkopen` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `klant` varchar(50) NOT NULL,
  `type` enum('pin','contant') NOT NULL,
  `price` double NOT NULL,
  `time` varchar(50) NOT NULL,
  `producten` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabelstructuur voor tabel `voorraad`
--
-- Aangemaakt: 20 okt 2020 om 12:15
--

DROP TABLE IF EXISTS `voorraad`;
CREATE TABLE `voorraad` (
  `org_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `ammount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `active`
--
ALTER TABLE `active`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `afwezigheid`
--
ALTER TABLE `afwezigheid`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `belasting`
--
ALTER TABLE `belasting`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `lonen`
--
ALTER TABLE `lonen`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `meldingen`
--
ALTER TABLE `meldingen`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `mutatieLogs`
--
ALTER TABLE `mutatieLogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `mutaties`
--
ALTER TABLE `mutaties`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `notities`
--
ALTER TABLE `notities`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `organisation`
--
ALTER TABLE `organisation`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `organisationStats`
--
ALTER TABLE `organisationStats`
  ADD PRIMARY KEY (`org_id`);

--
-- Indexen voor tabel `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `pendingUserDelete`
--
ALTER TABLE `pendingUserDelete`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `planning`
--
ALTER TABLE `planning`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `sollicitatie`
--
ALTER TABLE `sollicitatie`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `userStats`
--
ALTER TABLE `userStats`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexen voor tabel `verkopen`
--
ALTER TABLE `verkopen`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `active`
--
ALTER TABLE `active`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118486;

--
-- AUTO_INCREMENT voor een tabel `afwezigheid`
--
ALTER TABLE `afwezigheid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT voor een tabel `belasting`
--
ALTER TABLE `belasting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT voor een tabel `bonus`
--
ALTER TABLE `bonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT voor een tabel `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT voor een tabel `lonen`
--
ALTER TABLE `lonen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=753;

--
-- AUTO_INCREMENT voor een tabel `meldingen`
--
ALTER TABLE `meldingen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2628;

--
-- AUTO_INCREMENT voor een tabel `mutatieLogs`
--
ALTER TABLE `mutatieLogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT voor een tabel `mutaties`
--
ALTER TABLE `mutaties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1561;

--
-- AUTO_INCREMENT voor een tabel `notities`
--
ALTER TABLE `notities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=630;

--
-- AUTO_INCREMENT voor een tabel `organisation`
--
ALTER TABLE `organisation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT voor een tabel `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT voor een tabel `pendingUserDelete`
--
ALTER TABLE `pendingUserDelete`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT voor een tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT voor een tabel `planning`
--
ALTER TABLE `planning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7273;

--
-- AUTO_INCREMENT voor een tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=321;

--
-- AUTO_INCREMENT voor een tabel `sollicitatie`
--
ALTER TABLE `sollicitatie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=494;

--
-- AUTO_INCREMENT voor een tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;

--
-- AUTO_INCREMENT voor een tabel `verkopen`
--
ALTER TABLE `verkopen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9527;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
