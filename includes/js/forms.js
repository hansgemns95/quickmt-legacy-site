/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function formhash(form, password) {
    var p = document.createElement("input");

    form.appendChild(p);
    p.name = "p";
    p.type = "hidden";
    p.value = hex_sha512(password.value);

    password.value = "";

    form.submit();
}

function regformhash(form, password_old, password_new, password_new2) {
    if (password_old.value == ''         ||
        password_new.value == ''     ||
        password_new2.value == '') {
        window.location.replace("/profiel/password_empty/");
        return false;
    }

    if (password_new.value.length < 6) {
        window.location.replace("/profiel/password_short/");
        form.password.focus();
        return false;
    }

    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    if (!re.test(password_new.value)) {
        window.location.replace("/profiel/password_criteria/");
        return false;
    }

    if (password_new.value != password_new2.value) {
        window.location.replace("/profiel/password_different/");
        return false;
    }

    var p = document.createElement("input");
    var p1 = document.createElement("input");

    form.appendChild(p);
    p.name = "p";
    p.type = "hidden";
    p.value = hex_sha512(password_old.value);
    password_old.value = "";

    form.appendChild(p1);
    p1.name = "p1";
    p1.type = "hidden";
    p1.value = hex_sha512(password_new.value);
    password_new.value = "";

    password_new2.value = "";

    form.submit();
    return true;
}