<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function getAllMedwVanDeMaandInfo(){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT id,name, medwVanDeMaandPunten FROM user LEFT JOIN userStats on id = user_id WHERE medwVanDeMaandPunten != 0 ORDER BY medwVanDeMaandPunten DESC")) {
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function berekenMedwVanDeMaandPunten($userid){
    if (isAdmin($userid)) {
        setMedwVanDeMaandPunten($userid, 0);
        userMedwVanDeMaandChecked($userid);
        return;
    }

    if(PRIVATE_MEDWISEXCLUSED($userid)){
        setMedwVanDeMaandPunten($userid, 0);
        userMedwVanDeMaandChecked($userid);
        return;
    }

    $punten = 1;
    $userInfo = getUserInfo($userid);

    /*
     *  Elke speler krijgt 25x zijn level als punten
     */
    $punten += $userInfo['level'] * 25;

    /*
     *  Totale omzet / Eigen omzet
     */
    $totaleOmzet = getAllOmzetFromAllOrgsLast30Days() + 1;
    $totaleUserOmzet = getAllUserOmzetFromAllOrgsLast30Days($userid);
    $gemiddeld = round($totaleUserOmzet/$totaleOmzet, 0);
    $punten += $gemiddeld * 40;

    /*
     *  Totale activiteit / Eigen activiteit
     */
    $totalePlanningen = getTotaalAantalPlanningenAfgelopen30Dagen() + 1;
    $totaleUserPlanningen = getAantalPlanningenAfgelopen30Dagen($userid);
    $gemiddeld = round($totaleUserPlanningen/$totalePlanningen, 0);
    $punten += $gemiddeld * 25;

    /*
     *  Medewerker van de maand punten updaten
     */
    setMedwVanDeMaandPunten($userid, $punten);
    userMedwVanDeMaandChecked($userid);
}

function PRIVATE_MEDWISEXCLUSED($userid){
    $return = false;
    foreach (getAllOrganisations() as $organisation) {
        if($return) continue;
        if (!isActiveOrg($organisation['id'])) continue;
        if(!hasAccess($organisation['id'], $userid)) continue;
        if (hasPerms($organisation['id'], $userid, "loon.percentage.0")) {
            $return = true;
        }elseif (hasPerms($organisation['id'], $userid, "loon.percentage.125")) {
            $return = true;
        }
    }
    return $return;
}