<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function getLastCodeReset($userid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT lastCodeReset FROM userStats WHERE user_id = ? LIMIT 1")) {
        $stmt->bind_param('i', $userid);
        $stmt->execute();
        $stmt->store_result();

        $stmt->bind_result($lastCodeReset);
        $stmt->fetch();

        return $lastCodeReset;
    }
    return time();
}

function canResetCode($userid){
    $strotime = strtotime("+ 1 month", getLastCodeReset($userid));
    if(time() > $strotime){
        return true;
    }else{
        return false;
    }
}

function resetCode($userid){
    global $mysqli;
    $newCode = mt_rand(10000000, 99999999);
    $stmt = $mysqli->prepare("UPDATE user SET code = ? WHERE id = ?");
    $stmt->bind_param('ii', $newCode,$userid);
    $stmt->execute();
    $now = time();
    $stmt = $mysqli->prepare("UPDATE userStats SET lastCodeReset = ? WHERE user_id = ?");
    $stmt->bind_param('si', $now, $userid);
    $stmt->execute();
    return $newCode;
}

function getUserInfo($userid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM user LEFT JOIN userStats on id = user_id WHERE id = ? ")) {
        $stmt->bind_param('i', $userid);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return array();
}

function updateName($userid, $name){
    global $mysqli;
    $name = strip_tags($name);
    $stmt = $mysqli->prepare("UPDATE user SET name = ? WHERE id = ?");
    $stmt->bind_param('si', $name,$userid);
    $stmt->execute();
    $now = time();
    $stmt = $mysqli->prepare("UPDATE userStats SET lastNameUpdate = ? WHERE user_id = ?");
    $stmt->bind_param('si', $now,$userid);
    $stmt->execute();
}

function deleteUser($userid){
    global $mysqli;
    $active = 0;
    $stmt = $mysqli->prepare("UPDATE user SET active = ? WHERE id = ?");
    $stmt->bind_param('ii', $active, $userid);
    $stmt->execute();
    $now = time();
    $adminID = $_SESSION['user_id'];
    $stmt = $mysqli->prepare("UPDATE userStats SET deletedOn = ?, deletedBy = ? WHERE user_id = ?");
    $stmt->bind_param('sii', $now,$adminID,$userid);
    $stmt->execute();
    $stmt = $mysqli->prepare("DELETE FROM userOrganisation WHERE user_id = ?");
    $stmt->bind_param('i', $userid);
    $stmt->execute();
    $stmt = $mysqli->prepare("DELETE FROM userPerms WHERE user_id = ?");
    $stmt->bind_param('i', $userid);
    $stmt->execute();
}

function isAdmin($userid){
    global $mysqli;
    $admin = 1;
    if ($stmt = $mysqli->prepare("SELECT admin FROM user WHERE id = ? AND admin = ? LIMIT 1")) {
        $stmt->bind_param('ii', $userid, $admin);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }
    }
    return false;
}

function isPendingDeleted($userid,$org){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT user_id FROM pendingUserDelete WHERE org_id = ? AND user_id = ? LIMIT 1")) {
        $stmt->bind_param('ii', $org,$userid);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }
    }
    return false;
}

function setAdmin($userid, $status){
    global $mysqli;
    if($status == 1 || $status == 0){
        $stmt = $mysqli->prepare("UPDATE user SET admin = ? WHERE id = ?");
        $stmt->bind_param('ii', $status,$userid);
        $stmt->execute();
    }
}


function userExistsByUserID($userid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT username FROM user WHERE id = ? LIMIT 1")) {
        $stmt->bind_param('i', $userid);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }
    }
    return false;
}

function userExistsByUsername($username){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT username FROM user WHERE username = ? LIMIT 1")) {
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }
    }
    return false;
}



function getAllUsers(){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM user LEFT JOIN userStats on id = user_id")) {
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function createUser($username, $name, $password){
    global $mysqli;
    $password = hash('sha512', $password);
    $password = password_hash($password, PASSWORD_BCRYPT);
    $newCode = mt_rand(10000000, 99999999);
    $stmt = $mysqli->prepare("INSERT INTO user (username, password, code, name) VALUES (?,?,?,?)");
    $stmt->bind_param('ssis', $username, $password, $newCode, $name);
    $stmt->execute();
    $now = time();
    $id = getUserIDFromName($username)['id'];
    $adminID = $_SESSION['user_id'];
    $stmt = $mysqli->prepare("INSERT INTO userStats (user_id,createdOn, createdBy, lastCodeReset, lastNameUpdate,lastPasswordUpdate) VALUES (?,?,?,?,?,?)");
    $stmt->bind_param('isisss', $id,$now,$adminID,$now,$now,$now);
    $stmt->execute();
}

function resetPassword($userid, $password){
    global $mysqli;
    $password = hash('sha512', $password);
    $password = password_hash($password, PASSWORD_BCRYPT);
    $stmt = $mysqli->prepare("UPDATE user SET password = ? WHERE id = ?");
    $stmt->bind_param('si', $password, $userid);
    $stmt->execute();
    $now = time();
    $stmt = $mysqli->prepare("UPDATE userStats SET lastPasswordUpdate = ? WHERE user_id = ?");
    $stmt->bind_param('si', $now, $userid);
    $stmt->execute();
}

function getUserInfoFromName($username){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM user LEFT JOIN userStats on id = user_id WHERE username = ?")) {
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return array();
}

function getUserIDFromName($username){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT id FROM user WHERE username = ?")) {
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return array();
}


function isActiveUser($userid){
    global $mysqli;
    $active = 1;
    if ($stmt = $mysqli->prepare("SELECT username FROM user WHERE id = ? AND active = ? LIMIT 1")) {
        $stmt->bind_param('ii', $userid, $active);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }
    }
    return false;
}

function totaalAantalGebruikers(){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT name FROM user")) {
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows;
    }
    return 0;
}

function totaalMislukteInlogpogingen($minuten){
    $time = time();
    $time -= ($minuten * 60);
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts_ip WHERE time > ?")) {
        $stmt->bind_param('i', $time);
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows;
    }
    return 0;
}


function reActivateUser($userid){
    global $mysqli;
    $active = 1;
    $stmt = $mysqli->prepare("UPDATE user SET active = ? WHERE id = ?");
    $stmt->bind_param('ii', $active, $userid);
    $stmt->execute();
    $zero = 0;
    $stmt = $mysqli->prepare("UPDATE userStats SET deletedOn = ?, deletedBy = ? WHERE user_id = ?");
    $stmt->bind_param('sii', $zero,$zero,$userid);
    $stmt->execute();
}

function userLevelChecked($userid){
    global $mysqli;
    $now = time();
    $stmt = $mysqli->prepare("UPDATE userStats SET lastLevelCheck = ? WHERE user_id = ?");
    $stmt->bind_param('si', $now,$userid);
    $stmt->execute();
}

function userMedwVanDeMaandChecked($userid){
    global $mysqli;
    $now = time();
    $stmt = $mysqli->prepare("UPDATE userStats SET lastMedwVanDeMaandPuntenCheck = ? WHERE user_id = ?");
    $stmt->bind_param('si', $now,$userid);
    $stmt->execute();
}

function setMedwVanDeMaandPunten($userid, $punten){
    global $mysqli;
    $stmt = $mysqli->prepare("UPDATE userStats SET medwVanDeMaandPunten = ? WHERE user_id = ?");
    $stmt->bind_param('si', $punten,$userid);
    $stmt->execute();
}

function setLevel($userid, $level){
    global $mysqli;
    $stmt = $mysqli->prepare("UPDATE userStats SET level = ? WHERE user_id = ?");
    $stmt->bind_param('si', $level,$userid);
    $stmt->execute();
}

function userAllowedToUpdateMedwVanDeMaandPunten($userid){
    global $mysqli;
    $oneDayBack = time() - (60*60*24);
    if ($stmt = $mysqli->prepare("SELECT lastMedwVanDeMaandPuntenCheck FROM userStats WHERE user_id = ? AND lastMedwVanDeMaandPuntenCheck < ? LIMIT 1")) {
        $stmt->bind_param('ii', $userid, $oneDayBack);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }
    }
    return false;
}

function userAllowedToCheckLevel($userid){
    global $mysqli;
    $oneDayBack = time() - (60*60);
    if ($stmt = $mysqli->prepare("SELECT lastLevelCheck FROM userStats WHERE user_id = ? AND lastLevelCheck < ? LIMIT 1")) {
        $stmt->bind_param('ii', $userid, $oneDayBack);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }
    }
    return false;
}

function getPersoonlijkDoel($org, $user){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT challenge FROM personalChallenge WHERE org_id = ? AND user_id = ?")) {
        $stmt->bind_param('ii', $org,$user);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        if(!empty($allRows)){
            return $allRows[0];
        }else{
            return array('challenge'=>0);
        }
    }
    return array('challenge'=>0);
}

function setPersoonlijkeDoel($org,$user,$doel){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT challenge FROM personalChallenge WHERE org_id = ? AND user_id = ? LIMIT 1")) {
        $stmt->bind_param('ii', $org,$user);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt = $mysqli->prepare("UPDATE personalChallenge SET challenge = ? WHERE org_id = ? AND user_id = ?");
            $stmt->bind_param('dii', $doel,$org,$user);
            $stmt->execute();
            return;
        }
    }
    $stmt = $mysqli->prepare("INSERT INTO personalChallenge VALUES (?,?,?)");
    $stmt->bind_param('iis', $org,$user,$doel);
    $stmt->execute();
    return;
}

function setUserActive($org, $user){
    global $mysqli;
    $time = time();
    $stmt = $mysqli->prepare("INSERT INTO active (org_id, user_id, time) VALUES (?,?,?)");
    $stmt->bind_param('iis', $org,$user,$time);
    $stmt->execute();
}

function getUserActivety($org, $user, $start, $end){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM active WHERE org_id = ? AND user_id = ? AND time > ? AND time < ?")) {
        $stmt->bind_param('iiii', $org,$user,$start,$end);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function userWasActive($org, $user, $start, $end){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM active WHERE org_id = ? AND user_id = ? AND time > ? AND time < ? LIMIT 1")) {
        $stmt->bind_param('iiii', $org,$user,$start,$end);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }
    }
    return false;
}

function userIsALlowedToBypassChrome($userid){
    if(getUserInfo($userid)['requiredToUseChrome'] === 0){
        return true;
    }
    return false;
}

function getAmmountOfUserOrgs($userid){
    $ammount = 0;
    foreach (getAllOrganisations() as $org) {
        if (!hasAccess($org['id'], $userid)) continue;
        if ($org['active'] != 1) continue;
        if ($org['active'] == 1) {
            $ammount += 1;
        }
    }
    return $ammount;
}