<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

include_once 'userInfo.php';

function getTotaalAantalPermissions(){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM permissions")) {
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows;
    }
    return 0;
}

function hasPerms($orgid, $userid, $perm){
    if(isAdmin($userid)){
        return true;
    }
    if($perm != "*" && hasPerms($orgid, $userid, "*")){
        return true;
    }
    global $mysqli;
    if(isPartner($orgid)){
        if ($stmt = $mysqli->prepare("SELECT * FROM userPerms WHERE user_id = ?  AND org_id = ? AND perm_id = (SELECT id FROM permissions WHERE name = ? AND partner = 1) LIMIT 1")) {
            $stmt->bind_param('iis', $userid , $orgid, $perm);
            $stmt->execute();
            $stmt->store_result();
            if ($stmt->num_rows == 1) {
                return true;
            }
        }
    }else{
        if ($stmt = $mysqli->prepare("SELECT * FROM userPerms WHERE user_id = ?  AND org_id = ? AND perm_id = (SELECT id FROM permissions WHERE name = ? AND intern = 1) LIMIT 1")) {
            $stmt->bind_param('iis', $userid , $orgid, $perm);
            $stmt->execute();
            $stmt->store_result();
            if ($stmt->num_rows == 1) {
                return true;
            }
        }
    }
    return false;
}


function hasPermsByID($orgid, $userid, $permid){
    if(isAdmin($userid)){
        return true;
    }
    if(hasPerms($orgid,$userid,"*")){
        return true;
    }
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM userPerms WHERE user_id = ?  AND org_id = ? AND perm_id = ? LIMIT 1")) {
        $stmt->bind_param('iii', $userid , $orgid, $permid);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }
    }
    return false;
}
function getAllPerms(){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM permissions")) {
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function userPermAdd($userid, $orgid, $permid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("INSERT INTO userPerms (user_id, org_id, perm_id) VALUES (?,?,?)")) {
        $stmt->bind_param('iii', $userid , $orgid, $permid);
        $stmt->execute();
    }
}

function userPermRemove($userid, $orgid, $permid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("DELETE FROM userPerms WHERE user_id = ? AND org_id = ? AND perm_id = ?")) {
        $stmt->bind_param('iii', $userid , $orgid, $permid);
        $stmt->execute();
    }
}


function getPerm($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM permissions WHERE id = ? LIMIT 1")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return array();
}

function deleteAllUserPerms($orgid, $userid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("DELETE FROM userPerms WHERE user_id = ? AND org_id = ?")) {
        $stmt->bind_param('ii', $userid , $orgid);
        $stmt->execute();
    }
}