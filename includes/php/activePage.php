<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function getActivePage($request){
    if(!isset($request[0])){
        return "home";
    }else {
        switch ($request[0]) {
            case 'admin':
                if(isset($request[1]) && ($request[1] == "org" || $request[1] == "users" || $request[1] == "logs")){
                    return "admin";
                }
                return "home";
                break;
            case 'org':
                if($request[1] == 'verkoop') return "verkoop";
                if($request[1] == 'planning') return "planning";
                if($request[1] == 'voorraad') return "voorraad";
                if($request[1] == 'notities') return "notities";
                if($request[1] == 'afwezigheid') return "afwezigheid";
                if($request[1] == 'admin'){
                    if(isset($request[2]) && ($request[2] == "users" || $request[2] == "pages" || $request[2] == "products")){
                        return "admin";
                    }
                }
                if($request[1] == 'overzicht'){
                    if(isset($request[2]) && ($request[2] == "voorraad" || $request[2] == "sollicitatie"|| $request[2] == "afwezigheid"|| $request[2] == "verkopen"|| $request[2] == "mededelingen"|| $request[2] == "activiteit"|| $request[2] == "bonus"|| $request[2] == "doel" || $request[2] == "omzet" || $request[2] == "inkomsten_uitgaven" || $request[2] == "lonen")){
                        return "overzicht";
                    }
                }
                return 'home';
            case 'leaderboard':
                return "leaderboard";
            default:
                return "home";
                break;
        }
    }
}
