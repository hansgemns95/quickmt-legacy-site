<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function getLanguages($key, $section){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM language WHERE `key` = ? AND section = ?")) {
        $stmt->bind_param('ss', $key,$section);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        $value =$allRows[0]['value'];

        //RED
        $value = str_replace("{{red}}", "<span class=\"text-danger\">", $value);
        $value = str_replace("{{/red}}", "</span>", $value);

        //ITALIC
        $value = str_replace("{{italic}}", "<span class=\"font-italic\">", $value);
        $value = str_replace("{{/italic}}", "</span>", $value);

        //BOLD
        $value = str_replace("{{bold}}", "<span class=\"font-weight-bold\">", $value);
        $value = str_replace("{{/bold}}", "</span>", $value);

        //Break Line
        $value = str_replace("{{br}}", "<br/>", $value);

        return $value;
    }
    return "Error";
}

function updateLanguage($key, $newValue){
    global $mysqli;
    if ($stmt = $mysqli->prepare("UPDATE language SET value = ? WHERE key = ?")) {
        $stmt->bind_param('ss', $newValue, $key);
        $stmt->execute();
        $stmt->store_result();
    }
}