<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function getNotities($org){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM notities WHERE org_id = ? AND deletedOn = 0 ORDER BY createdOn DESC")) {
        $stmt->bind_param('i', $org);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getNotitie($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM notities WHERE id = ?")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return array();
}

function addNotitie($org, $user, $titel, $text){
    global $mysqli;
    $now = time();
    if ($stmt = $mysqli->prepare("INSERT INTO notities (org_id, user_id, titel, text, createdOn) VALUES (?,?,?,?,?)")) {
        $stmt->bind_param('iisss', $org,$user,$titel,$text, $now);
        $stmt->execute();
        $stmt->store_result();
    }
}

function deleteNotitie($id, $user){
    global $mysqli;
    $now = time();
    if ($stmt = $mysqli->prepare("UPDATE notities SET deletedOn = ?, deletedBy = ? WHERE id = ?")) {
        $stmt->bind_param('sii', $now, $user,$id);
        $stmt->execute();
        $stmt->store_result();
    }
}