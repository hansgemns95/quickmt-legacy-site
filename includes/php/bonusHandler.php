<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function addBonus($org, $user, $omschrijving, $bedrag){
    global $mysqli;
    $now = time();
    if ($stmt = $mysqli->prepare("INSERT INTO bonus (org_id, user_id, time, total, omschrijving) VALUES (?,?,?,?,?)")) {
        $stmt->bind_param('iisds', $org,$user,$now,$bedrag,$omschrijving);
        $stmt->execute();
        $stmt->store_result();
    }
}
function removeBonus($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("DELETE FROM bonus WHERE id = ?")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();
    }
}
function getBonussen_Bedrag($org, $user,$maxTime){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT SUM(total) as total FROM bonus WHERE org_id = ? AND user_id = ? AND time > ?")) {
        $stmt->bind_param('iis', $org, $user, $maxTime);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        if(isset($allRows[0])){
            return $allRows[0];
        }else{
            return array('total'=>0);
        }
    }
    return array('total'=>0);
}
function getBonussen($org){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM bonus WHERE org_id = ? ORDER BY time DESC")) {
        $stmt->bind_param('i', $org);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}


function bonusExists($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM bonus WHERE id = ? LIMIT 1")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows != 1) {
            return false;
        }else{
            return true;
        }
    }
}

function getBonus($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM bonus WHERE id = ?")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return array();
}
