<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function getPlanningSlot($org,$time, $slot){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT user_id FROM planning  WHERE org_id = ? AND time = ? AND slot_id = ?")) {
        $stmt->bind_param('isi', $org,$time,$slot);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}
function isClaimedSlot($org,$time,$slot){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM planning WHERE org_id = ? AND time = ? AND slot_id = ? AND user_id != 0 LIMIT 1")) {
        $stmt->bind_param('isi', $org, $time,$slot);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows != 1) {
            return false;
        }else{
            return true;
        }
    }
}

function slotExists($org, $time,$slot){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM planning WHERE org_id = ? AND time = ? AND slot_id = ? LIMIT 1")) {
        $stmt->bind_param('isi', $org, $time,$slot);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows != 1) {
            return false;
        }else{
            return true;
        }
    }
}

function claimPlanningSlot($org,$time,$slot,$user){
    if(slotExists($org,$time,$slot)){
        global $mysqli;
        $stmt = $mysqli->prepare("UPDATE planning SET user_id = ? WHERE org_id = ? AND time = ? AND slot_id = ?");
        $stmt->bind_param('iisi', $user,$org, $time,$slot);
        $stmt->execute();
    }else{
        global $mysqli;
        $stmt = $mysqli->prepare("INSERT INTO planning (org_id, user_id,time,slot_id) VALUES (?,?,?,?)");
        $stmt->bind_param('iisi', $org,$user, $time,$slot);
        $stmt->execute();
    }
}
function unclaimPlanningSlot($org,$time,$slot){
    global $mysqli;
    $stmt = $mysqli->prepare("UPDATE planning SET user_id = 0 WHERE org_id = ? AND time = ? AND slot_id = ?");
    $stmt->bind_param('isi', $org, $time,$slot);
    $stmt->execute();
}

function getAantalPlanningenAfgelopenWeek($userid){
    global $mysqli;
    $time = time() - (60*60*24*7);
    if ($stmt = $mysqli->prepare("SELECT * FROM planning WHERE user_id = ? AND time > ?")) {
        $stmt->bind_param('ii', $userid,$time);
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows;
    }
    return 0;
}

function getAantalPlanningenAfgelopen30Dagen($userid){
    global $mysqli;
    $time = time() - (60*60*24*30);
    if ($stmt = $mysqli->prepare("SELECT * FROM planning WHERE user_id = ? AND time > ?")) {
        $stmt->bind_param('ii', $userid,$time);
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows;
    }
    return 0;
}

function getTotaalAantalPlanningenAfgelopen30Dagen(){
    global $mysqli;
    $time = time() - (60*60*24*30);
    if ($stmt = $mysqli->prepare("SELECT * FROM planning WHERE time > ? AND user_id != 0")) {
        $stmt->bind_param('i',$time);
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows;
    }
    return 0;
}

function unclaimAllFutureSlotsFromDeletedUser($org,$user){
    global $mysqli;
    $time = strtotime('yesterday midnight');
    $stmt = $mysqli->prepare("UPDATE planning SET user_id = 0 WHERE org_id = ? AND user_id = ? AND time > ?");
    $stmt->bind_param('iis', $org, $user,$time);
    $stmt->execute();
}