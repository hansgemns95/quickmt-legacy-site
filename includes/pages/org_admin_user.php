<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php require_once './includes/modules/org_admin_users_header.php'; ?>
<?php if(!(isset($request[3]) && is_numeric($request[3]) && userExistsByUserID($request[3]) && hasAccess($_SESSION['org'], $request[3]))){ ?>
<?php require_once './includes/modules/org_admin_users_table.php'; ?>
<?php }else{ ?>
    <?php
    if(isset($request[4],$request[5])){
        if(hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.admin.users.manage")){
            $userid = $request[3];
            $permid = $request[4];
            $allow = true;
            if(isPartner($_SESSION['org']) && getPerm($permid)['partner'] == 0){
                $allow = false;
            }
            if(!isPartner($_SESSION['org']) && getPerm($permid)['intern'] == 0){
                $allow = false;
            }

            if($allow && $request[5] == "enable" && hasPermsByID($_SESSION['org'], $_SESSION['user_id'], $permid) && $_SESSION['user_id'] != $userid && !isAdmin($userid)){
                if(!hasPermsByID($_SESSION['org'],$userid, $permid)){
                    userPermAdd($userid, $_SESSION['org'], $permid);
                    $org = getOrganisation($_SESSION['org']);
                    $user = getUserInfo($userid);
                    $perm = getPerm($permid);
                    addLog($_SESSION['user_id'], "Succesvolle voor de organisatie ". $org["name"] . " (".$org['location'].") voor de gebruiker ".$user['name']." (".$user['username'].") de permissie " . $perm['name'] . " toegevoegd");
                }
            }
            if($allow && $request[5] == "disable" && hasPermsByID($_SESSION['org'], $_SESSION['user_id'], $permid) && $_SESSION['user_id'] != $userid && !isAdmin($userid)){
                if(hasPermsByID($_SESSION['org'],$userid, $permid)){
                    userPermRemove($userid, $_SESSION['org'], $permid);
                    $org = getOrganisation($_SESSION['org']);
                    $user = getUserInfo($userid);
                    $perm = getPerm($permid);
                    addLog($_SESSION['user_id'], "Succesvolle voor de organisatie ". $org["name"] . " (".$org['location'].") voor de gebruiker ".$user['name']." (".$user['username'].") de permissie " . $perm['name'] . " verwijderd");
                }
            }
            echo "<script> window.location.replace('/org/admin/users/".$userid."/') </script>";
        }else{
            echo "<script> window.location.replace('/org/admin/users/') </script>";
        }
    }
    ?>
<?php if(hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.admin.users.manage") && !isPendingDeleted($request[3], $_SESSION['org'])){?>
        <?php require_once './includes/modules/org_admin_users_loon.php'; ?>
<?php require_once './includes/modules/org_admin_users_perms.php'; ?>
<?php }else{ ?>
<?php require_once './includes/modules/org_admin_users_table.php'; ?>
<?php } ?>
<?php } ?>
