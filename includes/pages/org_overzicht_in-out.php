<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php require_once './includes/modules/org_overzicht_in-out_header.php';?>
<?php if (isset($_POST['omschrijving'],$_POST['bedrag'],$_POST['type'])) {
    $omschrijving = $_POST['omschrijving'];
    $bedrag = $_POST['bedrag'];
    $bedrag = str_replace(",", ".", $bedrag);
    $type = $_POST['type'];
    if($type == 'in' || $type == 'out'){
        $omschrijving = strip_tags($omschrijving);
        if(hasPerms($_SESSION['org'], $_SESSION['user_id'], 'page.overzicht.in_out.add') && !empty($omschrijving) && is_numeric($bedrag)){
            $bedrag = round($bedrag,1);
            addMutatie($_SESSION['org'], $type, $bedrag, $omschrijving);
            $_SESSION['overzicht_in_out_added'] = $type;
        }
    }
} ?>
<?php if (isset($request[3],$request[4], $request[5]) && $request[3] == 'delete') {
    $mutatieID = $request[4];
    if(hasPerms($_SESSION['org'], $_SESSION['user_id'], 'page.overzicht.in_out.remove') && is_numeric($mutatieID) && ($request[5] == 'in' || $request[5] == 'out') && mutatieExists($mutatieID)){
        if(date("Y-m", time()) === date("Y-m", getMutatie($mutatieID)['time'])){
            removeMutatie($mutatieID);
            $_SESSION['overzicht_in_out_removed'] = $request[5];
        }

    }
} ?>
<div class="row">
    <div class="col-xl-6 col-lg-6">
        <?php require_once './includes/modules/org_overzicht_in-out_in.php'; ?>
    </div>
    <div class="col-xl-6 col-lg-6">
        <?php require_once './includes/modules/org_overzicht_in-out_out.php'; ?>
    </div>
</div>
