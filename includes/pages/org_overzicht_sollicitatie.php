<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php require_once './includes/modules/org_overzicht_sollicitatie_header.php'; ?>

<?php if (isset($request[3],$request[4])) {
    $sollicitatieID = $request[3];
    $newStatus = $request[4];
    if($newStatus == "archive" && sollicitatieExists($sollicitatieID) && getSollicitatie($sollicitatieID)[0]['status'] == "pending" && hasPerms($_SESSION['org'], $_SESSION['user_id'], 'page.overzicht.sollicitatie')){
        updateSollicitatieStatus($sollicitatieID, "archive", $_SESSION['user_id']);
        addLog($_SESSION['user_id'], "Succesvolle de sollicitatie van " . getSollicitatie($sollicitatieID)[0]['username']. " behandeld (status: archived).");
        $_SESSION['sollicitatie_handled'] = 'true';
    }

    if(!isset($_SESSION['sollicitatie_handled'])){
        $_SESSION['sollicitatie_handled'] = 'false';
    }
    echo '<script>window.location.href = "/org/overzicht/sollicitatie/";</script>';
}else{ ?>

    <?php require_once './includes/modules/org_overzicht_sollicitatie_users.php'; ?>
<?php } ?>



