<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php require_once './includes/modules/admin_mededelingen_header.php'; ?>

<?php if (isset($_POST['users'],$_POST['prioriteit'],$_POST['icon'],$_POST['omschrijving'])) {
    $users= $_POST['users'];
    $omschrijving = $_POST['omschrijving'];
    $omschrijving = strip_tags($omschrijving);
    $prioriteit = $_POST['prioriteit'];
    $prioriteit = strip_tags($prioriteit);
    $icon = $_POST['icon'];
    $icon = strip_tags($icon);
    if(isset($_POST['kopieToAuthor'])){
        $kopieToAuthor = true;
    }else{
        $kopieToAuthor= false;
    }

    $meldingID = 'Mededeling::'.time()."::".rand(111111111,999999999);
    if(!empty($omschrijving) && !empty($users) && isAdmin($_SESSION['user_id'])){
        foreach ($users as $user){
            if (isAdmin($user['id'])) continue;
            if($user == 0) continue;
            if($user == $_SESSION['user_id']) continue;
            createMelding($user, $_SESSION['user_id'], $prioriteit, $icon, $omschrijving,$meldingID);
        }
        if($kopieToAuthor){
            createMelding($_SESSION['user_id'], $_SESSION['user_id'], $prioriteit, $icon, $omschrijving,$meldingID);
        }
        $_SESSION['mededelingen_verstuurd'] = 'true';
    }

    if(!isset($_SESSION['mededelingen_verstuurd'])){
        $_SESSION['mededelingen_verstuurd'] = 'false';
    }
    echo '<script>window.location.href = "/admin/mededelingen/";</script>';
} ?>




<div class="row">
    <div class="col-xl-6 col-lg-6">
        <?php require_once './includes/modules/admin_mededelingen_sent.php'; ?>
    </div>
    <div class="col-xl-6 col-lg-6">
        <?php require_once './includes/modules/admin_mededelingen_list.php'; ?>
    </div>
</div>
