<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

if(isset($request[3],$request[4])){
    if(hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.admin.pages.manage")){
        $pageID = $request[3];
        $allow = true;
        if(isPartner($_SESSION['org']) && getPage($pageID)['partner'] == 0){
          $allow = false;
        }
        if(!isPartner($_SESSION['org']) && getPage($pageID)['intern'] == 0){
            $allow = false;
        }
        if($request[4] == "enable" && $allow){
            if(!orgHasPage($_SESSION['org'],$pageID)){
                orgPageEnable($_SESSION['org'], $pageID);
                $org = getOrganisation($_SESSION['org']);
                addLog($_SESSION['user_id'], "Succesvolle voor de organisatie ". $org["name"] . " (".$org['location'].") de pagine ".$pageID." geactiveerd");
            }
        }
        if($request[4] == "disable" && $allow){
            if(orgHasPage($_SESSION['org'],$pageID)) {
                orgPageDisable($_SESSION['org'], $pageID);
                $org = getOrganisation($_SESSION['org']);
                addLog($_SESSION['user_id'], "Succesvolle voor de organisatie ". $org["name"] . " (".$org['location'].") de pagine ".$pageID." gedeactiveerd");
            }
        }
    }
    echo "<script> window.location.replace('/org/admin/pages/') </script>";
}
?>
<?php require_once './includes/modules/org_admin_page_header.php'; ?>
<?php require_once './includes/modules/org_admin_page_selector.php'; ?>
