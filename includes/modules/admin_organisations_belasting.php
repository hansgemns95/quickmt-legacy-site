<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?>

<?php $org = getOrganisation($request[2]); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Organisatie Omzet Belasting % Aanpassen</h6>
    </div>
    <div class="card-body">
        <?php if (empty($org)){ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De omzet belasting kan niet worden geladen
                </div>
            </div>
        <?php } elseif(isset($request[2])&&$request[2]  == "1") { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    U kunt het admin account niet aanpassen
                </div>
            </div>
        <?php } elseif(!isActiveOrg($request[2])) { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De organisatie kan niet worden aangepast aangezien deze is verwijderd
                </div>
            </div>
        <?php } elseif(isset($request[3])&&$request[3]  == "percentage_changed") { ?>
            <div class="card bg-success text-white shadow">
                <div class="card-body">
                    Het omzet % is aangepast naar:
                    <span class="font-weight-bold"><?php print $org['belastingPercentage']; ?></span>
                </div>
            </div>
        <?php } elseif(isset($request[3])&&$request[3]  == "percentage_error") { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    Er is een fout opgetreden bij het aanpassen van het omzet percentage
                </div>
            </div>
        <?php } else { ?>
            <form method="POST" action="/includes/auth/process_orgbelastingchange.php" name="namechange_form">
                <p><span class="font-weight-bold">Huidige Omzet Percentage</span> <?php print getOrgBelastingPercentage($org['id'])."%"; ?></p>
                <p class="font-italic">Het is belangrijk dat u een geheel getal invult tussen de 0 en de 100%</p>
                <div class="form-group">
                    <input autocomplete="off" type="text" class="form-control form-control-user" name="percentage" id="name" value="<?php print getOrgBelastingPercentage($org['id']); ?>" placeholder="Omzet Percentage" required>
                </div>
                <button type="submit"  value="<?php print $org['id']; ?>" name="org" class="btn btn-primary btn-user btn-block">Aanpassen</button>
            </form>
        <?php } ?>
    </div>
</div>