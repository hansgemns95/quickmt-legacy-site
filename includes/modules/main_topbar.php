<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

$userInfo = getUserInfo($_SESSION['user_id']);
$meldingen = getOngelezenMeldingen($userInfo['id']);
$bestellingen = getOpenBestellingen($userInfo['id']);
?>
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <p class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100"><?php print getOrganisation($_SESSION['org'])['name']; ?> - <?php print getOrganisation($_SESSION['org'])['location']; ?>    |    <?php print getUserInfo($_SESSION['user_id'])['username']; ?> (<?php print getUserInfo($_SESSION['user_id'])['name']; ?>)
        <?php if(!isPartner($_SESSION['org'])) { ?>
        | Level <?php print getUserInfo($_SESSION['user_id'])['level']?>
        <?php } ?>
    </p>

    <ul class="navbar-nav ml-auto">

        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <span <?php if(getAantalOngelezenMelding($userInfo['id']) < 1){print "hidden"; } ?> class="badge badge-danger badge-counter"><?php print getAantalOngelezenMelding($userInfo['id']); ?></span>
            </a>
            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                    Meldingen
                </h6>
                <?php
                if(empty($meldingen)){ ?>
                    <a class="dropdown-item d-flex align-items-center">
                        <div>
                            Er zijn momenteel geen ongelezen meldingen
                        </div>
                    </a>
                <?php }else{ ?>
                    <?php foreach ($meldingen as $melding){ ?>
                        <a class="dropdown-item d-flex align-items-center" href="/melding/<?php print $melding['id']; ?>/">
                            <div class="mr-3">
                                <?php if($melding['prioriteit'] == "urgent"){ ?>
                                    <div class="icon-circle bg-danger">
                                        <i class="fas <?php print $melding['icon'] ?> text-white"></i>
                                    </div>
                                <?php }else if($melding['prioriteit'] == "high"){ ?>
                                    <div class="icon-circle bg-warning">
                                        <i class="fas <?php print $melding['icon'] ?> text-white"></i>
                                    </div>
                                <?php }else if($melding['prioriteit'] == "medium"){ ?>
                                    <div class="icon-circle bg-primary">
                                        <i class="fas <?php print $melding['icon'] ?> text-white"></i>
                                    </div>
                                <?php }else if($melding['prioriteit'] == "low"){ ?>
                                    <div class="icon-circle bg-info">
                                        <i class="fas <?php print $melding['icon'] ?> text-white"></i>
                                    </div>
                                <?php } ?>
                            </div>
                            <div>
                                <div class="small text-gray-500"><?php print date("F j, Y", $melding['created_on']); ?></div>
                                <?php if(strlen($melding['message']) > 100){ ?>
                                    <?php print substr($melding['message'], 0, 100) . '...'; ?>
                                <?php }else{ ?>
                                    <?php print $melding['message']; ?>
                                <?php } ?>
                            </div>
                        </a>
                    <?php } ?>
                <?php } ?>
                <a class="dropdown-item text-center small text-gray-500" href="/melding/">Geef alle meldingen weer</a>
            </div>
        </li>

        <?php if(!isPartner($_SESSION['org'])){ ?>
            <li class="nav-item dropdown no-arrow mx-1">
                <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-envelope fa-fw"></i>
                    <span <?php if(getAantalOpenBestelling($userInfo['id']) < 1){print "hidden"; } ?> class="badge badge-danger badge-counter"><?php print getAantalOpenBestelling($userInfo['id']); ?></span>
                </a>
                <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                    <h6 class="dropdown-header">
                        Openstaande Bestellingen
                    </h6>
                    <?php
                    if(empty($bestellingen)){ ?>
                        <a class="dropdown-item d-flex align-items-center">
                            <div>
                                Er zijn momenteel geen openstaande bestellingen
                            </div>
                        </a>
                    <?php }else{ ?>
                        <?php foreach ($bestellingen as $bestelling){ ?>
                            <a class="dropdown-item d-flex align-items-center" href="/bestelling/<?php print 1; ?>/">
                                <div>
                                    <div class="text-truncate">Quick Food</div>
                                    <div class="small text-gray-500">10 minuten</div>
                                </div>
                            </a>
                        <?php } ?>
                    <?php } ?>
                    <a class="dropdown-item text-center small text-gray-500" href="/bestelling/">Toon alle bestellingen</a>
                </div>
            </li>
        <?php } ?>

        <div class="topbar-divider d-none d-sm-block"></div>

        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="/profiel/" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-800 small"><?php print $userInfo['name']; ?></span>
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400 d-block d-lg-none" style="font-size: 175%"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="/profiel/">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Profiel
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="" data-toggle="modal" data-target="#orgModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Ander Bedrijf
                </a>
                <a class="dropdown-item" href="" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                </a>
            </div>
        </li>

    </ul>
</nav>