<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?>

<?php $org = getOrganisation($request[2]); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary"><?php print getLanguages("admin_organisations_information", "org_info");?></h6>
    </div>
    <div class="card-body">
        <?php if ($org['id'] >= 1){ ?>
            <p><span class="font-weight-bold"><?php print getLanguages("admin_organisations_information", "org_info_id");?></span> <?php print $org['id']; ?></p>
            <p><span class="font-weight-bold"><?php print getLanguages("admin_organisations_information", "org_info_name");?></span> <?php print $org['name']; ?></p>
            <p><span class="font-weight-bold"><?php print getLanguages("admin_organisations_information", "org_info_location");?></span> <?php print $org['location']; ?></p>
            <p><span class="font-weight-bold">Partner: </span> <?php if($org['partner'] == 1){print "Ja";}else{print "Nee";} ?></p>
            <?php if(isPartner($org['id'])){ ?>
                <p><span class="font-weight-bold">Partner Omzet Percentage: </span> <?php print getOrgBelastingPercentage($org['id']) . "%"; ?></p>
            <?php } ?>
            <?php if($org['active'] != 1){ ?>
                <p><span class="font-weight-bold"><?php print getLanguages("admin_organisations_information", "org_info_deleted_on");?></span> <?php print date("d-m-Y H:i:s", $org['deletedOn']); ?></p>
                <p><span class="font-weight-bold"><?php print getLanguages("admin_organisations_information", "org_info_deleted_by");?></span> <?php print getUserInfo($org['deletedBy'])['username'] . " (".getUserInfo($org['deletedBy'])['name'].")"; ?></p>
            <?php }else{ ?>
                <p><span class="font-weight-bold"><?php print getLanguages("admin_organisations_information", "org_info_created_on");?></span> <?php print date("d-m-Y H:i:s", $org['createdOn']); ?></p>
                <p><span class="font-weight-bold"><?php print getLanguages("admin_organisations_information", "org_info_created_by");?></span> <?php print getUserInfo($org['createdBy'])['username'] . " (".getUserInfo($org['createdBy'])['name'].")"; ?></p>
                <p><span class="font-weight-bold"><?php print getLanguages("admin_organisations_information", "org_info_last_change_name");?></span> <?php print date("d-m-Y H:i:s", $org['lastNameChange']); ?></p>
                <p><span class="font-weight-bold"><?php print getLanguages("admin_organisations_information", "org_info_last_change_location");?></span> <?php print date("d-m-Y H:i:s", $org['lastLocationChange']); ?></p>
                <p><span class="font-weight-bold"><?php print getLanguages("admin_organisations_information", "org_info_last_change_icon");?></span> <?php print date("d-m-Y H:i:s", $org['lastIconChange']); ?></p>
            <?php } ?>
        <?php } else { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    <?php print getLanguages("admin_organisations_information", "org_info_cannot_load");?>
                </div>
            </div>
        <?php } ?>
        <?php if(isActiveOrg($org['id']) && $org['id'] != 1){ ?>
                <form method="POST" action="/includes/auth/process_deleteorg.php" class="user" name="adminOrgCreate_form" id="adminOrgCreate_form">
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-primary shadow-sm" name="org" id="org" value="<?php print $org['id']; ?>"><?php print getLanguages("admin_organisations_information", "org_delete");?></button>
                    </div>
                </form>
        <?php } ?>

    </div>
</div>