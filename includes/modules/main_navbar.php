<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="quickSidebar">

    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/home/">
        <div class="sidebar-brand-icon">
            <img src="/includes/img/vogel.png" height="55px">
        </div>
        <div class="sidebar-brand-text mx-3">QuickMT <sup>V<?php print $_VERSION; ?></sup></div>
    </a>

    <hr class="sidebar-divider my-0">
    <li class="nav-item <?php if($pagina == "home"){print " active";}?>">
        <a class="nav-link" href="/home/">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>


    <?php if(!isPartner($_SESSION['org'])) { ?>
        <li class="nav-item <?php if($pagina == "leaderboard"){print " active";}?>">
            <a class="nav-link" href="/leaderboard/">
                <i class="fas fa-fw fa-list-ol"></i>
                <span>Scorelijst</span></a>
        </li>
    <?php } ?>

    <hr class="sidebar-divider my-0">

    <?php if(orgHasPage($_SESSION['org'], 1) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.verkoop") && !isAdmin($_SESSION['user_id'])){ ?>
    <li class="nav-item <?php if($pagina == "verkoop"){print " active";}?>">
        <a class="nav-link" href="/org/verkoop/">
            <i class="fas fa-fw fa-euro-sign"></i>
            <span>Verkoop</span></a>
    </li>
    <?php } ?>

    <?php if(orgHasPage($_SESSION['org'], 2) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.voorraad") && !isAdmin($_SESSION['user_id'])){ ?>
        <li class="nav-item <?php if($pagina == "voorraad"){print " active";}?>">
            <a class="nav-link" href="/org/voorraad/">
                <i class="fas fa-fw fa-box-open"></i>
                <span>Voorraad</span></a>
        </li>
    <?php } ?>

    <?php if(orgHasPage($_SESSION['org'], 7) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.notities") && !isAdmin($_SESSION['user_id'])){ ?>
        <li class="nav-item <?php if($pagina == "notities"){print " active";}?>">
            <a class="nav-link" href="/org/notities/">
                <i class="fas fa-fw fa-clipboard"></i>
                <span>Notities</span></a>
        </li>
    <?php } ?>

    <?php if(orgHasPage($_SESSION['org'], 13) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.planning") && !isAdmin($_SESSION['user_id'])){ ?>
        <li class="nav-item <?php if($pagina == "planning"){print " active";}?>">
            <a class="nav-link" href="/org/planning/">
                <i class="fas fa-fw fa-calendar"></i>
                <span>Planning</span></a>
        </li>
    <?php } ?>

    <?php if(orgHasPage($_SESSION['org'], 15) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.afwezigheid") && !isAdmin($_SESSION['user_id'])){ ?>
        <li class="nav-item <?php if($pagina == "afwezigheid"){print " active";}?>">
            <a class="nav-link" href="/org/afwezigheid/">
                <i class="fas fa-fw fa-plane"></i>
                <span>Afwezigheid</span></a>
        </li>
    <?php } ?>

    <?php if(
        orgHasPage($_SESSION['org'], 3) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.voorraad")  && !isAdmin($_SESSION['user_id']) ||
        orgHasPage($_SESSION['org'], 5) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.verkopen")  && !isAdmin($_SESSION['user_id']) ||
        orgHasPage($_SESSION['org'], 6) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.lonen")  && !isAdmin($_SESSION['user_id'])||
        orgHasPage($_SESSION['org'], 8) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.in_out") && !isAdmin($_SESSION['user_id']) ||
        orgHasPage($_SESSION['org'], 9) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.doel") && !isAdmin($_SESSION['user_id']) ||
        orgHasPage($_SESSION['org'], 12) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.mededelingen") && !isAdmin($_SESSION['user_id']) ||
        orgHasPage($_SESSION['org'], 10) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.bonus") && !isAdmin($_SESSION['user_id']) ||
        orgHasPage($_SESSION['org'], 11) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.activity") && !isAdmin($_SESSION['user_id']) ||
        orgHasPage($_SESSION['org'], 14) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.sollicitatie") && !isAdmin($_SESSION['user_id']) ||
        orgHasPage($_SESSION['org'], 16) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.afwezigheid") && !isAdmin($_SESSION['user_id']) ||
        orgHasPage($_SESSION['org'], 4) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.omzet") && !isAdmin($_SESSION['user_id'])){ ?>
    <li class="nav-item <?php if($pagina == "overzicht"){print " active";}?>">
        <a class="nav-link collapsed" href="/org/" data-toggle="collapse" data-target="#overzichtCollapse" aria-expanded="true" aria-controls="overzichtCollapse">
            <i class="fas fa-fw fa-search"></i>
            <span>Overzicht</span>
        </a>
        <div id="overzichtCollapse" class="collapse" aria-labelledby="overzichtCollapse" data-parent="#quickSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <?php if(orgHasPage($_SESSION['org'], 3) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.voorraad") && !isAdmin($_SESSION['user_id'])) { ?>
                    <a class="collapse-item" href="/org/overzicht/voorraad/">Voorraad</a>
                <?php } ?>
                <?php if(orgHasPage($_SESSION['org'], 5) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.verkopen") && !isAdmin($_SESSION['user_id'])) { ?>
                    <a class="collapse-item" href="/org/overzicht/verkopen/">Verkopen</a>
                <?php } ?>
                <?php if(orgHasPage($_SESSION['org'], 4) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.omzet") && !isAdmin($_SESSION['user_id'])) { ?>
                    <a class="collapse-item" href="/org/overzicht/omzet/">Omzet</a>
                <?php } ?>
                <?php if(orgHasPage($_SESSION['org'], 6) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.lonen") && !isAdmin($_SESSION['user_id'])) { ?>
                    <a class="collapse-item" href="/org/overzicht/lonen/">Lonen</a>
                <?php } ?>
                <?php if(orgHasPage($_SESSION['org'], 8) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.in_out") && !isAdmin($_SESSION['user_id'])) { ?>
                    <a class="collapse-item" href="/org/overzicht/inkomsten_uitgaven/">Inkomsten & uitgaven</a>
                <?php } ?>
                <?php if(orgHasPage($_SESSION['org'], 9) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.doel") && !isAdmin($_SESSION['user_id'])) { ?>
                    <a class="collapse-item" href="/org/overzicht/doel/">Persoonlijke Doelen</a>
                <?php } ?>
                <?php if(orgHasPage($_SESSION['org'], 10) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.bonus") && !isAdmin($_SESSION['user_id'])) { ?>
                    <a class="collapse-item" href="/org/overzicht/bonus/">Bonus</a>
                <?php } ?>
                <?php if(orgHasPage($_SESSION['org'], 11) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.activity") && !isAdmin($_SESSION['user_id'])) { ?>
                    <a class="collapse-item" href="/org/overzicht/activiteit/">Activiteiten</a>
                <?php } ?>
                <?php if(orgHasPage($_SESSION['org'], 14) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.sollicitatie") && !isAdmin($_SESSION['user_id'])) { ?>
                    <a class="collapse-item" href="/org/overzicht/sollicitatie/">Sollicitaties</a>
                <?php } ?>
                <?php if(orgHasPage($_SESSION['org'], 16) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.afwezigheid") && !isAdmin($_SESSION['user_id'])) { ?>
                    <a class="collapse-item" href="/org/overzicht/afwezigheid/">Afwezigheid</a>
                <?php } ?>
                <?php if(orgHasPage($_SESSION['org'], 12) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.mededelingen") && !isAdmin($_SESSION['user_id'])) { ?>
                    <a class="collapse-item" href="/org/overzicht/mededelingen/">Mededelingen</a>
                <?php } ?>

            </div>
        </div>
    </li>
    <?php } ?>

    <?php if(isAdmin($_SESSION['user_id']) && $_SESSION['org'] == 1){ ?>
    <hr class="sidebar-divider my-0">
    <li class="nav-item <?php if($pagina == "admin"){print " active";}?>">
        <a class="nav-link collapsed" href="/home/" data-toggle="collapse" data-target="#adminCollapse" aria-expanded="true" aria-controls="adminCollapse">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Admin</span>
        </a>
        <div id="adminCollapse" class="collapse" aria-labelledby="adminCollapse" data-parent="#quickSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="/admin/users/">Gebruikers</a>
                <a class="collapse-item" href="/admin/org/">Organisaties</a>
                <a class="collapse-item" href="/admin/logs/">Logboek</a>
            </div>
        </div>
    </li>
    <?php } elseif(
        hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.users")||
        hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.pages")||
        hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.products")){ ?>
        <hr class="sidebar-divider my-0">
        <li class="nav-item <?php if($pagina == "admin"){print " active";}?>">
            <a class="nav-link collapsed" href="/home/" data-toggle="collapse" data-target="#adminCollapse" aria-expanded="true" aria-controls="adminCollapse">
                <i class="fas fa-fw fa-wrench"></i>
                <span>Admin</span>
            </a>
            <div id="adminCollapse" class="collapse" aria-labelledby="adminCollapse" data-parent="#quickSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <?php if(hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.users")) { ?>
                        <a class="collapse-item" href="/org/admin/users/">Gebruikers</a>
                    <?php } ?>
                    <?php if(hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.pages")) { ?>
                        <a class="collapse-item" href="/org/admin/pages/">Pagina's</a>
                    <?php } ?>
                    <?php if(hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.products")) { ?>
                        <a class="collapse-item" href="/org/admin/products/">Producten</a>
                    <?php } ?>
                </div>
            </div>
        </li>
    <?php } ?>

    <hr class="sidebar-divider d-none d-md-block">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>