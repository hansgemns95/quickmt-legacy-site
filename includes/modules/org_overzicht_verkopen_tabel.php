<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php if(isset($_SESSION['verkoop_deleted']) && $_SESSION['verkoop_deleted'] == "true") { unset($_SESSION['verkoop_deleted']);  ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-success text-white shadow mb-3">
                <div class="card-body">
                    De geselecteerde verkoop is succesvol verwijderd
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($_SESSION['verkoop_deleted']) && $_SESSION['verkoop_deleted'] == "false") { unset($_SESSION['verkoop_deleted']);  ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    De verkoop kon niet worden verwijderd
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php $users = getAllUsersFromOrg($_SESSION['org'])?>
<?php foreach ($users as $user) { ?>
    <?php if(isAdmin($user['id'])) continue; ?>
    <?php $transacties = getAllVerkopenFromUser($_SESSION['org'], $user['id']);?>
    <?php if(!empty($transacties)){ ?>
        <div class="card shadow mb-4">
            <a href="#lonen_calc_<?php print $user['name'] ?>" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="lonen_calc_<?php print $user['name'] ?>">
                <h6 class="m-0 font-weight-bold text-primary"><?php print $user['name'] ?></h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse hide" id="lonen_calc_<?php print $user['name'] ?>">
                <div class="card-body">
                    <div class="row">
                        <div class="table">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 15%">Datum</th>
                                    <th style="width: 15%">Medewerker</th>
                                    <th style="width: 15%">Klant</th>
                                    <th style="width: 10%">Betaaltype</th>
                                    <th style="width: 10%">Bedrag</th>
                                    <th style="width: 35%">Producten</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($transacties as $transactie){ ?>
                                    <tr>
                                        <td><?php print date("d-m-Y H:i:s", $transactie['time']); ?></td>
                                        <td><?php print $user['username']; ?> (<?php print $user['name']; ?>)</td>
                                        <td><?php print $transactie['klant']; ?></td>
                                        <td><?php print $transactie['type']; ?></td>
                                        <td>€<?php print $transactie['price']; ?></td>
                                        <td><?php
                                            $producten = json_decode($transactie['producten'], true);
                                            foreach ($producten as $productid => $aantal){
                                                print getProductByID($productid)['name'] . " : " . $aantal . "<br>";
                                            }
                                            ?></td>
                                        <td>
                                            <?php if($transactie['time'] < getOrganisation($_SESSION['org'])['lastLoonReset'] || !hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.overzicht.verkopen.manage")){ ?>
                                                <a class="btn btn-sm btn-secondary shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                            <?php } else { ?>
                                                <a href="/org/overzicht/verkopen/<?php print $transactie['id']; ?>/del/" class="btn btn-sm btn-danger shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>