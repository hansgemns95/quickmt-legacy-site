<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><h1 class="h3 mb-4 text-gray-800">Door jou verstuurde aanvragen</h1>
<?php $afwezigheden = getAfwezigheden($_SESSION['user_id'],$_SESSION['org']);?>
<?php foreach ($afwezigheden as $afwezigheid){ ?>
    <?php
    if($afwezigheid['status'] == 'in behandeling'){
        $status = '<span class="font-weight-bold text-info">In Behandeling</span>';
    }elseif($afwezigheid['status'] == 'geaccepteerd'){
        $status = '<span class="font-weight-bold text-success">Geaccepteerd</span>';
    }else{
        $status = '<span class="font-weight-bold text-danger">Geweigerd</span>';
    }
    ?>
    <div class="card shadow mb-4">
        <a href="#melding_<?php print $afwezigheid['id'] ?>" class="d-block card-header py-3 collpsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="melding_<?php print $afwezigheid['id'] ?>">
            <h6 class="m-0 font-weight-bold text-primary">
                Afwezigheid Aanvraag (<?php print date("d-m-Y", $afwezigheid['start_date']); ?> t/m <?php print date("d-m-Y", $afwezigheid['end_date']); ?>) - <?php print $status; ?><br>
            </h6>
        </a>
        <div class="collapse hide" id="melding_<?php print $afwezigheid['id'] ?>">
            <div class="card-body">
                <span class="h5"><?php print $afwezigheid['reason']; ?></span>
            </div>
            <?php if($afwezigheid['handled_by'] !== 0 && $afwezigheid['status'] !== 'in behandeling'){ ?>
            <div class="card-footer">
                <span class="h7 font-weight-bold">Verwerkt Door</span><br>
                <span class="h9"><?php print getUserInfo($afwezigheid['handled_by'])['name']; ?></span><br>
            </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>