<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $userInfo = getUserInfo($_SESSION['user_id']); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Minecraft Naam Aanpassen</h6>
    </div>
    <div class="card-body">
        <?php if (empty($userInfo)){ ?>
        <div class="card bg-danger text-white shadow">
            <div class="card-body">
                Uw profiel kan momenteel niet worden geladen
            </div>
        </div>
        <?php } elseif(isset($request[1])&&$request[1]  == "name_changed") { ?>
            <div class="card bg-success text-white shadow">
                <div class="card-body">
                    Uw naam is succesvol aangepast naar: <span class="font-weight-bold"><?php print $userInfo['name']; ?></span>
                </div>
            </div>
        <?php } elseif(isset($request[1])&&$request[1]  == "name_error") { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    Uw naam kon niet worden aangepast
                </div>
            </div>
        <?php } elseif(isset($request[1])&&$request[1]  == "name_empty") { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De ingevoerde naam is leeg, probeer het later nog eens
                </div>
            </div>
        <?php } else { ?>
            <form method="POST" action="/includes/auth/process_namechange.php" name="namechange_form">
                <p><span class="font-weight-bold">Minecraftnaam:</span> <?php print $userInfo['name']; ?></p>
                <div class="form-group">
                    <input autocomplete="off" type="text" class="form-control form-control-user" name="name" id="name" value="<?php print $userInfo['name']; ?>" placeholder="Nieuwe Naam" required>
                </div>
                <input type="button"  value="Aanpassen" class="btn btn-primary btn-user btn-block" onclick="this.form.submit()">
            </form>
        <?php }?>
    </div>
</div>