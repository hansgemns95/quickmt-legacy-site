<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php if(isset($request[1]) && is_numeric($request[1]) && meldingExists($request[1]) && userCanReadMelding($_SESSION['user_id'], $request[1])){ ?>
    <?php $melding = getMelding($request[1]); ?>
    <?php setMeldingAlsGelezen($request[1]); ?>
    <?php $creator = getUserInfo($melding['created_by']); ?>
    <?php if($creator == 0){
        $creator = "Systeem";
    }else{
        $creator = $creator['name'];
    }?>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Melding (Prioriteit: <?php print strtoupper($melding['prioriteit']); ?>)</h6>
        </div>
        <div class="card-body">
            <?php print $melding['message']; ?>
        </div>
        <div class="card-footer">
            <?php print date("F j, Y", $melding['created_on']); ?> |  Verzonden door: <?php  print $creator; ?> | Dit bericht is verstuurd naar <?php print getHoeveelheidDezelfdeVerstuurdeMelding($melding['melding_group_id']); ?> personen
        </div>
    </div>

<?php }else{ ?>
    <?php $meldingen = getAlleMeldingen($_SESSION['user_id']); ?>
    <?php if(empty($meldingen)){ ?>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Melding</h6>
            </div>
            <div class="card-body">
                <span class="h5">U heeft momenteel geen openstaande meldingen</span>
            </div>

        </div>
    <?php } ?>
    <?php foreach ($meldingen as $melding){ ?>
        <?php $creator = getUserInfo($melding['created_by']); ?>
        <?php if($creator == 0){
            $creator = "Systeem";
        }else{
            $creator = $creator['name'];
        }?>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                    <?php if($melding['gelezen'] == 0) { ?>
                        <span class="m-0 font-weight-bold text-primary"> Ongelezen </span>
                    <?php } ?>
                    Melding (Prioriteit: <?php print strtoupper($melding['prioriteit']); ?>)
                </h6>
            </div>
            <div class="card-body">
                <?php print $melding['message']; ?>
            </div>
            <div class="card-footer">
                <?php print date("F j, Y", $melding['created_on']); ?> |  Verzonden door: <?php  print $creator; ?> | Dit bericht is verstuurd naar <?php print getHoeveelheidDezelfdeVerstuurdeMelding($melding['melding_group_id']); ?> personen
            </div>
        </div>
        <?php setMeldingAlsGelezen($melding['id']); ?>
    <?php } ?>
<?php } ?>

