<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?>
<?php $logs = getLogs($request[2]);?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="font-weight-bold text-primary">Logboek</h6>
    </div>
    <div class="card-body">
        <?php if(!empty($logs)){ ?>
            <div class="table">
                <table class="table table-bordered" id="logboekTable">
                    <thead>
                    <tr>
                        <th style="width: 20%">Datum</th>
                        <th style="width: 80%">Actie</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($logs as $log){ ?>
                        <tr>
                            <td><?php print date("d-m-Y H:i:s", $log['time']); ?></td>
                            <td><?php print $log['value']; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php }else{ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    Het logboek kan momenteel niet worden geladen
                </div>
            </div>
        <?php } ?>
    </div>
</div>