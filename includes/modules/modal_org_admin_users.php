<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="modal fade" id="addUserToOrg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Gebruiker Toevoegen</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="/includes/auth/process_addusertoorg.php" class="user" name="UserAddToOrg_form" id="UserAddToOrg_form">
                    <div class="form-group">
                        <select required class="form-control" name="useridtoorg">
                            <?php foreach (getAllUsers() as $user){ ?>
                                <?php if(isAdmin($user['id'])) continue; ?>
                                <?php if($user['active'] == 0) continue; ?>
                                <?php if (!hasAccess($_SESSION['org'], $user['id'])) { ?>
                                    <option value="<?php print $user['id']; ?>"><?php print $user['name']; ?> (<?php print $user['username']; ?>)</option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuleren</button>
                <a class="btn btn-primary text-white" onclick="document.getElementById('UserAddToOrg_form').submit();">Toevoegen</a>
            </div>
        </div>
    </div>
</div>