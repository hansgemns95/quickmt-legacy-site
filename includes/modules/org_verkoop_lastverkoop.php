<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

$lastVerkoop = getAllVerkopenFromUser($_SESSION['org'], $_SESSION['user_id'])[0];
$lastLoonCalculate = getOrganisation($_SESSION['org'])['lastLoonReset'];
if(isset($lastVerkoop) && !empty($lastVerkoop) && $lastVerkoop['time'] > $lastLoonCalculate){ 

$productenFormatted = json_decode($lastVerkoop['producten'], true);
$product = "";

foreach ($productenFormatted as $productid => $aantal){
    $product .= "<li><span class=\"font-weight-bold\">" . getProductByID($productid)['name'] . ": </span> " . $aantal ."</li>";
}
    
?> 

<hr>
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Laatste Verkoop</h1>
</div>
<p>
    <span class="font-weight-bold">Klant: </span> <?php print $lastVerkoop['klant']; ?> <br>
    <span class="font-weight-bold">Betaalmethode: </span> <?php print $lastVerkoop['type']; ?> <br>
    <span class="font-weight-bold">Bedrag: </span> <?php print $lastVerkoop['price']; ?> <br>
    <span class="font-weight-bold">Datum: </span> <?php print date("d-m-Y H:i", $lastVerkoop['time']); ?> <br>
    <span class="font-weight-bold">Producten: <br>
    <ul>
        <?php print $product; ?>
    </ul>
</p>
<?php } ?>
