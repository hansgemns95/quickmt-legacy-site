<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><hr>
<div class="card shadow mb-4">
    <!-- Card Header - Accordion -->
    <a href="#lonen_last2" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="lonen_last2">
        <h6 class="m-0 font-weight-bold text-primary">Laatste Berekening</h6>
    </a>
    <!-- Card Content - Collapse -->
    <div class="collapse hide" id="lonen_last2">
        <div class="card-body">
            <div class="row">
                <?php $users = getAllUsers();?>
                <?php if(!empty($users)){ ?>
                    <div class="table">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 10%">ID</th>
                                <th style="width: 20%">Username</th>
                                <th style="width: 20%">Naam</th>
                                <th style="width: 50%">Loon</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($users as $user){ ?>
                            <?php if(isAdmin($user['id'])) continue; ?>
                            <?php if(getAllLastLoonFromUser($_SESSION['org'], getOrganisation($_SESSION['org'])['lastLoonReset'], $user['id'])[0]['total'] == 0) continue; ?>
                            <?php $lonen = getAllLoonFromUser($_SESSION['org'],$user['id']); ?>
                            <?php if(!empty($lonen)){ ?>
                                <tr>
                                    <td><?php print $user['id']; ?></td>
                                    <td><?php print $user['username']; ?></td>
                                    <td><?php print $user['name']; ?></td>
                                    <td>€<?php print getAllLastLoonFromUser($_SESSION['org'], getOrganisation($_SESSION['org'])['lastLoonReset'], $user['id'])[0]['total'] + 0; ?></td>
                                </tr>
                            <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                <?php }else{ ?>
                    <div class="card bg-danger text-white shadow">
                        <div class="card-body">
                            De lonen kunnen momenteel niet worden geladen
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
