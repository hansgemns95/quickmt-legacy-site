<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?>

<?php $user = getUserInfo($request[2]); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary"><?php print getLanguages("admin_user_admin", "user_admin");?></h6>
    </div>
    <div class="card-body">
        <?php if (empty($user)){ ?>
        <div class="card bg-danger text-white shadow">
            <div class="card-body">
                <?php print getLanguages("admin_user_admin", "user_admin_cannot_load"); ?>
            </div>
        </div>
        <?php } elseif($user['id'] == $_SESSION['user_id']) { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    <?php print getLanguages("admin_user_admin", "user_admin_own"); ?>
                </div>
            </div>
        <?php } else { ?>
            <p><?php print getLanguages("admin_user_admin", "user_admin_info");?></p>
            <?php if($user['admin'] != 1){ ?>
                <form method="POST" action="/includes/auth/process_adminEnableUser.php" class="user" name="adminOrgCreate_form" id="adminOrgCreate_form">
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-success shadow-sm" name="user" id="user" value="<?php print $user['id']; ?>"><?php print getLanguages("admin_user_admin", "user_admin_enable");?></button>
                    </div>
                </form>
            <?php }else{ ?>
                <form method="POST" action="/includes/auth/process_adminDisableUser.php" class="user" name="adminOrgCreate_form" id="adminOrgCreate_form">
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-danger shadow-sm" name="user" id="user" value="<?php print $user['id']; ?>"><?php print getLanguages("admin_user_admin", "user_admin_disable");?></button>
                    </div>
                </form>
            <?php } ?>
        <?php } ?>
    </div>
</div>