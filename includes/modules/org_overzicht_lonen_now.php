<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php if(isset($_SESSION['lonen_calculated']) && $_SESSION['lonen_calculated'] == "true") { unset($_SESSION['lonen_calculated']); ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-success text-white shadow mb-3">
                <div class="card-body">
                    De lonen zijn berekend
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($_SESSION['lonen_calculated']) && $_SESSION['lonen_calculated'] == "false") { unset($_SESSION['lonen_calculated']); ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    De lonen zijn niet berekend
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<hr>
<div class="card shadow mb-4">
    <!-- Card Header - Accordion -->
    <a href="#lonen_last" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="lonen_last">
        <h6 class="m-0 font-weight-bold text-primary">Actueel</h6>
    </a>
    <!-- Card Content - Collapse -->
    <div class="collapse hide" id="lonen_last">
        <div class="card-body">
            <div class="row">
                <?php $users = getAllUsersFromOrg($_SESSION['org']);?>
                <?php if(!empty($users)){ ?>
                    <div class="table">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 10%">ID</th>
                                <th style="width: 20%">Username</th>
                                <th style="width: 20%">Naam</th>
                                <th style="width: 10%">%</th>
                                <th style="width: 40%">Loon</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($users as $user){ ?>
                                <?php if(isAdmin($user['id'])) continue; ?>
                                <?php $userBonus = getBonussen_Bedrag($_SESSION['org'], $user['id'], getOrganisation($_SESSION['org'])['lastLoonReset']); ?>
                                <tr>
                                    <td><?php print $user['id']; ?></td>
                                    <td><?php print $user['username']; ?></td>
                                    <td><?php print $user['name']; ?></td>
                                    <td><?php print getLoonPercentage($_SESSION['org'], $user['id']) * 100 . "%"; ?></td>
                                    <td>
                                        €<?php print round(getOmzetTotalPerUserSinceLastoonRest($_SESSION['org'], $user['id'], getOrganisation($_SESSION['org'])['lastLoonReset']) * getLoonPercentage($_SESSION['org'],$user['id']),2); ?>
                                        <?php if($userBonus['total'] > 0){ ?>
                                            <span > (+ €<?php print $userBonus['total']; ?> bonus)</span>
                                        <?php }else if($userBonus['total'] < 0){ ?>
                                            <span > (€<?php print $userBonus['total']; ?> bonus)</span>
                                        <?php } ?>
                                        <span class="text-danger">*</span><br>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                <?php }else{ ?>
                    <div class="card bg-danger text-white shadow">
                        <div class="card-body">
                            De lonen kunnen momenteel niet worden geladen
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="card-footer text-muted">
            <span class="text-danger">*</span> Deze bedragen zijn een indicatie. Deze worden pas definitief wanneer de lonen worden berekend
        </div>
    </div>
</div>
