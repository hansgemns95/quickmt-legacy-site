<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

if(isset( $_SESSION['planning_start'],$_SESSION['planning_end'])) {
    $startDay = $_SESSION['planning_start'];
    $endDay =$_SESSION['planning_end'];

    $i = 1;

    $time = $startDay + 57600;

    $users = getAllUsersFromOrg($_SESSION['org']);
    $inactiveUsers = array();

    foreach ($users as $user) {
        if (isInActive($_SESSION['org'], $user['id'], $time)) {
            $inactiveUsers[] = $user['name'];
        }
    } ?>

    <?php if(!empty($inactiveUsers)){ ?>
        <div class="row" id="vita_max">
            <div class="col-lg-6">
                <div class="card bg-warning text-white shadow mb-3">
                    <div class="card-body">
                        <span class="font-weight-bold">Let op!</span><br>
                        De volgende personen hebben voor vandaag een goedgekeurde afwezigheid.<br>
                        <ul>
                            <?php foreach ($inactiveUsers as $username) { ?>
                                <li><?php print $username; ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>


<div class="card shadow mb-4">
    <!-- Card Header - Accordion -->
    <a href="#activiteit_1" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="activiteit_1">
        <h6 class="m-0 font-weight-bold text-primary">Planning | <?php print date('d-m-Y', $_SESSION['planning_start'])?></h6>
    </a>
    <!-- Card Content - Collapse -->
    <div class="" id="activiteit_1">
        <div class="card-body">


            <div class="row">
                <div class="table">
                    <table class="table table-bordered">
                        <tbody>
                        <?php while($time < ($endDay - 7199)){ ?>
                            <tr>
                                <td style="width: 10%"><?php print date('H:i', $time)?> - <?php print date('H:i', $time + 899)?></td>
                                <?php $planningSlot = getPlanningSlot($_SESSION['org'], $_SESSION['planning_start'], $i); ?>
                                <?php if(!empty($planningSlot) && $planningSlot[0]['user_id'] != 0 && userExistsByUserID($planningSlot[0]['user_id'])){ ?>
                                    <td style="width: 90%" class="bg-success text-white" onclick="window.location='/org/planning/<?php print $i; ?>/claim/'"><?php print getUserInfo($planningSlot[0]['user_id'])['name']; ?></td>
                                <?php }else{ ?>
                                    <td style="width: 90%" onclick="window.location='/org/planning/<?php print $i; ?>/claim/'"></td>
                                <?php } ?>
                                <?php $i++; ?>
                            </tr>
                            <?php $time += 900; ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
}
?>
