<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="modal fade" id="adminOrgCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Organisatie Toevoegen</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="/includes/auth/process_createorg.php" class="user" name="adminOrgCreate_form" id="adminOrgCreate_form">
                    <div class="form-group">
                        <input autocomplete="off" type="text" class="form-control" name="name" id="name" placeholder="Organisatie Naam" required>
                    </div>
                    <div class="form-group">
                        <input autocomplete="off" type="text" class="form-control" name="location" id="location" placeholder="Locatie" required>
                    </div>
                    <div class="form-group">
                        <select autocomplete="off"  required class="form-control" name="partner">
                            <option value=1>Partner: Ja</option>
                            <option value=0>Partner: Nee</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input autocomplete="off" type="hidden" class="form-control" name="form" id="form" value="adminOrgCreate" placeholder="adminOrgCreate_form">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuleren</button>
                <a class="btn btn-primary text-white" onclick="document.getElementById('adminOrgCreate_form').submit();">Aanmaken</a>
            </div>
        </div>
    </div>
</div>