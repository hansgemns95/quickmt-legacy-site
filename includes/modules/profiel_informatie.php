<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $userInfo = getUserInfo($_SESSION['user_id']); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Persoonlijke Informatie</h6>
    </div>
    <div class="card-body">
        <?php if (!empty($userInfo) >= 1){ ?>
            <p><span class="font-weight-bold">Medewerker ID:</span> <?php print $userInfo['id']; ?></p>
            <p><span class="font-weight-bold">Username:</span> <?php print $userInfo['username']; ?></p>
            <p><span class="font-weight-bold">Minecraftnaam:</span> <?php print $userInfo['name']; ?></p>
            <p><span class="font-weight-bold">Persoonlijke Code:</span> <?php print $userInfo['code']; ?></p>
            <p><span class="font-weight-bold">Aangemaakt Op:</span> <?php print date("d-m-Y H:i:s", $userInfo['createdOn']); ?></p>
            <p><span class="font-weight-bold">Aangemaakt Door:</span> <?php print getUserInfo($userInfo['createdBy'])['username'] . " (".getUserInfo($userInfo['createdBy'])['name'].")"; ?></p>
            <p><span class="font-weight-bold">Laatste Code Reset:</span> <?php print date("d-m-Y H:i:s", $userInfo['lastCodeReset']); ?></p>
            <p><span class="font-weight-bold">Laatste Minecraftnaam Aanpassing:</span> <?php print date("d-m-Y H:i:s", $userInfo['lastNameUpdate']); ?></p>
            <p><span class="font-weight-bold">Laatste Wachtwoord Wijziging:</span> <?php print date("d-m-Y H:i:s", $userInfo['lastPasswordUpdate']); ?></p>
            <hr>
            Om deze informatie te wijzigen dient u contact op te nemen met uw leidinggevende
        <?php } else { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    Uw profiel kan momenteel niet worden geladen
                </div>
            </div>
        <?php } ?>
    </div>
</div>