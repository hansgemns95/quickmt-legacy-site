<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?>
<?php $users = getAllUsers();?>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="font-weight-bold text-primary">Gebruikers</h6>
        </div>
        <div class="card-body">
            <?php if(!empty($users)){ ?>
                <div class="table">
                            <table class="table table-bordered" id="adminOrgTable">
                                <thead>
                                <tr>
                                    <th style="width: 10%">ID</th>
                                    <th style="width: 20%">Username</th>
                                    <th style="width: 40%">Naam</th>
                                    <th style="width: 10%">Admin</th>
                                    <th style="width: 20%">Actief</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($users as $user){ ?>
                                <tr>
                                    <td ><?php print $user['id']; ?></td>
                                    <td ><?php print $user['username']; ?></td>
                                    <td ><?php print $user['name']; ?></td>
                                    <td ><?php if($user['admin'] == 1){print "Ja";}else{print "Nee";} ?></td>
                                    <td ><?php if($user['active'] == 1){print "Ja";}else{print "Nee";} ?></td>
                                    <?php if(!empty(getLogs($user['id']))){ ?>
                                        <td ><a href="/admin/logs/<?php print $user['id']; ?>/" class="btn btn-sm btn-primary shadow-sm"><i class="fas fa-search fa-lg text-white"></i></a></td>
                                    <?php }else{ ?>
                                        <td ><span class="btn btn-sm btn-danger shadow-sm disabled"><i class="fas fa-times fa-lg text-white"></i></span></td>
                                    <?php } ?>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
            <?php }else{ ?>
                <div class="card bg-danger text-white shadow">
                    <div class="card-body">
                        De gebruikers kunnen momenteel niet worden geladen
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>


