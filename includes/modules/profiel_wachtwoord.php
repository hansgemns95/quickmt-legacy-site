<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $userInfo = getUserInfo($_SESSION['user_id']); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Wachtwoord Wijzigen</h6>
    </div>
    <div class="card-body">
        <?php if (empty($userInfo)){ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    Uw profiel kan momenteel niet worden geladen
                </div>
            </div>
        <?php } elseif(isset($request[1])&&$request[1]  == "password_error") { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    Uw wachtwoord kon niet worden aangepast
                </div>
            </div>
        <?php } else { ?>
            <?php if(isset($request[1])&&$request[1]  == "password_empty") { ?>
                <div class="card bg-danger text-white shadow">
                    <div class="card-body">
                        Niet alle velden waren juist ingevuld
                    </div>
                </div>
                <br>
            <?php } elseif(isset($request[1])&&$request[1]  == "password_short") { ?>
                <div class="card bg-danger text-white shadow">
                    <div class="card-body">
                        Het wachtwoord moet uit minimaal 6 tekens bestaan
                    </div>
                </div>
                <br>
            <?php } elseif(isset($request[1])&&$request[1]  == "password_criteria") { ?>
                <div class="card bg-danger text-white shadow">
                    <div class="card-body">
                        Het wachtwoord moet bestaan uit minimaal 1 cijfer, 1 kleine letter en 1 hoofdletter
                    </div>
                </div>
                <br>
            <?php } elseif(isset($request[1])&&$request[1]  == "password_different") { ?>
                <div class="card bg-danger text-white shadow">
                    <div class="card-body">
                        De nieuwe wachtwoorden komen niet overeen
                    </div>
                </div>
                <br>
            <?php } elseif(isset($request[1])&&$request[1]  == "password_wrong") { ?>
                <div class="card bg-danger text-white shadow">
                    <div class="card-body">
                        Het oude wachtwoord is onjuist
                    </div>
                </div>
                <br>
            <?php }?>
            <form method="POST" action="/includes/auth/process_passwordchange.php" name="passchange_form">
                <div class="form-group">
                    <input autocomplete="off" type="password" class="form-control form-control-user" name="old" id="old" placeholder="Oude Wachtwoord">
                </div>
                <div class="form-group">
                    <input autocomplete="off" type="password" class="form-control form-control-user" name="new" id="new" placeholder="Nieuwe Wachtwoord">
                </div>
                <div class="form-group">
                    <input  autocomplete="off" type="password" class="form-control form-control-user" name="new1" id="new1" placeholder="Herhaling Nieuwe Wachtwoord">
                </div>
                <input type="button" class="btn btn-primary btn-user btn-block" value="Aanpassen" onclick="regformhash(this.form, this.form.old, this.form.new,this.form.new1);" />
            </form>
        <?php }?>
    </div>
</div>