<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $perms = getAllPerms();?>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="font-weight-bold text-primary">Permissies - <?php print getUserInfo($request[3])['username']; ?> (<?php print getUserInfo($request[3])['name']; ?>)</h6>
        </div>
        <div class="card-body">
            <?php if(!empty($perms)){ ?>
                <div class="table">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 25%">Naam</th>
                                    <th style="width: 60%">Omschrijving</th>
                                    <th style="width: 15%">Actief</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($perms as $perm){ ?>
                                    <?php if(isPartner($_SESSION['org']) && $perm['partner'] == 0) continue; ?>
                                    <?php if(!isPartner($_SESSION['org']) && $perm['intern'] == 0) continue; ?>
                                <tr>
                                    <td><?php print $perm['name']; ?></td>
                                    <td><?php print $perm['description']; ?></td>
                                    <td><?php if(hasPerms($_SESSION['org'], $request[3], "*") && $perm['name'] != "*"){print "Ja (*)";}elseif(hasPerms($_SESSION['org'], $request[3], $perm['name'])){print "Ja";}else{print "Nee";} ?></td>
                                    <td>
                                        <?php if((!hasPerms($_SESSION['org'],$_SESSION['user_id'], $perm['name'])) || $request[3] == $_SESSION['user_id'] || isAdmin($request[3])){ ?>
                                            <a class="btn btn-sm btn-secondary shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                        <?php }elseif(hasPerms($_SESSION['org'], $request[3], "*") && $perm['name'] != "*"){ ?>
                                            <a class="btn btn-sm btn-secondary shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                        <?php }elseif(hasPerms($_SESSION['org'], $request[3], $perm['name'])){ ?>
                                            <a href="/org/admin/users/<?php print $request[3]; ?>/<?php print $perm['id']; ?>/disable/" class="btn btn-sm btn-danger shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                        <?php } else { ?>
                                            <a href="/org/admin/users/<?php print $request[3]; ?>/<?php print $perm['id']; ?>/enable/" class="btn btn-sm btn-success shadow-sm"><i class="fas fa-check fa-lg text-white"></i></a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
            <?php }else{ ?>
                <div class="card bg-danger text-white shadow">
                    <div class="card-body">
                        De permissies kunnen momenteel niet worden geladen
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>


