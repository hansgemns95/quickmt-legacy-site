<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

ob_start();
include '../../errorHandler.php';
register_shutdown_function('shutdownErrorFunction', $_SESSION);

include_once 'db_connect.php';
include_once 'functions.php';
include_once '../php/userInfo.php';
include_once '../php/logHandler.php';

sec_session_start();

if(login_check($mysqli) != true) {
    header('Location: /error/');
    exit();
}

if (isset($_POST['p'], $_POST['p1'])) {
    $oud = $_POST['p'];
    $nieuw = $_POST['p1'];

    if(!validatePassword($_SESSION['user_id'], $oud)){
        header('Location: /profiel/password_wrong/');
        exit();
    }

    changePassword($_SESSION['user_id'],$nieuw);

    addLog($_SESSION['user_id'], "Uw wachtwoord is aangepast");

    header('Location: /password_reset/');
    exit();
}
header('Location: /profiel/password_error/');
exit();



