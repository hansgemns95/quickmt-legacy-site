<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

ob_start();
include '../../errorHandler.php';
register_shutdown_function('shutdownErrorFunction', $_SESSION);

include_once 'db_connect.php';
include_once 'functions.php';
include_once '../php/logHandler.php';
include_once '../php/orgHandler.php';
include_once '../php/userInfo.php';

sec_session_start();

if(login_check($mysqli) != true) {
    header('Location: /error/');
    exit();
}

if(isset($_POST['pwrdrst'])){
    if(!userExistsByUserID($_POST['pwrdrst'])){
        header('Location: /admin/users/');
        exit();
    }

    if(!isActiveUser($_POST['pwrdrst'])){
        header('Location: /admin/users/'.$_POST['pwrdrst'].'/');
        exit();
    }

    if(!isAdmin($_SESSION['user_id'])){
        header('Location: /admin/users/');
        exit();
    }

    if(isAdmin($_POST['pwrdrst'])){
        header('Location: /admin/users/'.$_POST['pwrdrst'].'/');
        exit();
    }

    if($_SESSION['user_id'] == $_POST['pwrdrst']){
        header('Location: /admin/users/'.$_POST['pwrdrst'].'/password_error/');
        exit();
    }

    $password = generateRandomString();

    $userinfo = getUserInfo($_POST['pwrdrst']);

    resetPassword($_POST['pwrdrst'], $password);

    $_SESSION['TMP_PWDRESET'] = $password;

    addLog($_SESSION['user_id'], "Succesvolle het wachtwoord gereset van de gebruiker ". $userinfo['username'] ." - (". $userinfo['name'] .") gemaakt");

    header('Location: /admin/users/'.$_POST['pwrdrst'].'/password_succes/');
    exit();
}

header('Location: /admin/users/'.$_POST['pwrdrst'].'/password_error/');
exit();


function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}